import copy

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn.functional as F
from loguru import logger

from disease_states import DiseaseState


class DiseaseGrids:
    def __init__(self,  
                        grids=None,
                        batch_size=100, grid_size=30, 
                        device="cuda" if torch.cuda.is_available() else "cpu",
                        ):
        """
        Initialize the DiseaseGrids class with batch size, grid size, and device.

        Parameters:
        batch_size (int): The number of grids to simulate in parallel.
        grid_size (int): The size of each grid (grid_size x grid_size).
        device (torch.device): The device to run the computations on: cpu, cuda, cuda:0, cuda:1 
        """
        self.batch_size = batch_size
        self.grid_size = grid_size
        self.device = device
        
        self.initialize_grids(grids)
        self.instantiate_disease_state_vectors()
        self.instantiate_infectious_neighbour_count_kernel()
        self.instantiate_function_output_cache()
        

    def initialize_grids(self, grids=None):
        """
        Initialize the grids with all cells set to EMPTY state.

        Returns:
        torch.Tensor: Initialized grids tensor.
        """
        if grids is not None:
            self.grids = grids.clone().to(torch.float)
            assert self.grids.shape == (self.batch_size, self.grid_size, self.grid_size, len(DiseaseState)), "Invalid grid shape provided."
            return self.grids
        
        # instantiate normally
        self.grids = torch.zeros(self.batch_size, self.grid_size, self.grid_size, len(DiseaseState), dtype=torch.float, device=self.device)
        # Set all cells to EMPTY state initially
        self.grids[:, :, :, DiseaseState.EMPTY.value] = 1
        return self.grids

    def instantiate_disease_state_vectors(self):
        """
        Create one-hot encoded vectors for each disease state.
        """
        self.disease_state_vectors = {}
        for _idx, disease_state in enumerate(DiseaseState):
            # Create one-hot vector for each disease state
            self.disease_state_vectors[disease_state] = (torch.arange(len(DiseaseState), device=self.device) == _idx).to(torch.float)
            logger.debug(f"{disease_state.name}: {self.disease_state_vectors[disease_state]}")

    def instantiate_infectious_neighbour_count_kernel(self):
        """
        Define the kernel for counting infectious neighbors using convolution.
        """
        infectious_neighbour_count_kernel = torch.ones(3, 3, device=self.device, dtype=torch.float)
        infectious_neighbour_count_kernel[1, 1] = 0  # Do not count the cell itself
        self.infectious_neighbour_count_kernel = infectious_neighbour_count_kernel.view(1, 1, 3, 3)

    def instantiate_function_output_cache(self):
        """
        Instantiate the function output cache.
        """
        self.function_output_cache = {
            "count_infected_neighbours": {
                "is_tainted": True,
                "value": None
            },
            "calculate_immediate_infection_risk_mask": {
                "is_tainted": True,
                "value": None
            },
            "calculate_optimal_vaccines_count": {
                "is_tainted": True,
                "value": None
            },
            "calculate_percentage_of_individuals_in_all_disease_states": {
                "is_tainted": True,
                "value": None
            },
            "get_infection_field": {
                "is_tainted": True,
                "value": None
            }
        } # a cache to keep a copy of the output of some functions
        
    def invalidate_cache(self):
        for _key in self.function_output_cache:
            self.function_output_cache[_key]["is_tainted"] = True
            
    def add_function_output_to_cache(self, function_name, value):
        self.function_output_cache[function_name]["value"] = value
        self.function_output_cache[function_name]["is_tainted"] = False

    def check_for_cache_hit(self, function_name):
        return not self.function_output_cache[function_name]["is_tainted"]
    
    def retrieve_from_cache(self, function_name):
        if not self.function_output_cache[function_name]["is_tainted"]:
            logger.debug("Cache hit for function: " + function_name)
            return self.function_output_cache[function_name]["value"]
        return False

    def set_state(self, mask, state):
        """
        Set the state of cells specified by the mask.

        Parameters:
        mask (torch.Tensor): Boolean mask specifying which cells to update.
        state (DiseaseState): The state to set for the specified cells.
        """
        self.grids[mask] = self.disease_state_vectors[state]
        self.invalidate_cache() # invalidate the whole function output cache

    def __getitem__(self, index):
        """
        Passthrough getter function
        """
        return self.grids[index]

    def __setitem__(self, index, value):
        """
        Passthrough setter function        
        """
        self.grids[index] = value
        self.invalidate_cache() # invalidate the whole function output cache

    def count_infected_neighbours(self):
        """
        Count the number of infectious neighbors for each cell.

        Returns:
        torch.Tensor: A tensor containing the count of infectious neighbors for each cell.
                      Dimension: batch_size x grid_size x grid_size
        """
        
        # check for cache hit # todo- rewrite these as decorators to avoid repetition
        if self.check_for_cache_hit("count_infected_neighbours"): return self.retrieve_from_cache("count_infected_neighbours")
        
        infected_layer = self.grids[:, :, :, DiseaseState.INFECTIOUS.value].unsqueeze(1)
        # Use circular padding to handle edge cases for toric grids
        # padded_infected_layer = F.pad(infected_layer, (1, 1, 1, 1), mode='circular') # toric mode
        
        padded_infected_layer = F.pad(infected_layer, (1, 1, 1, 1), mode='constant', value=0) # zero padding
        
        # Perform convolution to count infectious neighbors
        infectious_neighbours_count = F.conv2d(padded_infected_layer, self.infectious_neighbour_count_kernel).squeeze(1)
        
        # add entry to cache
        self.add_function_output_to_cache("count_infected_neighbours", infectious_neighbours_count)
        
        return infectious_neighbours_count

    def get_infection_field(self, infection_probability=1, recovery_probability=0, discount_factor=0.99, num_disease_steps=2, batch_subset_mask=None, collect_statistics=False, collect_optimal_vaccines_count=False):
        """
        """         
        
        # check for cache hit # todo- rewrite these as decorators to avoid repetition
        if self.check_for_cache_hit("get_infection_field"): return self.retrieve_from_cache("get_infection_field")
        
        if batch_subset_mask is not None:
            grids_clone = self.clone_grid(subset_mask=batch_subset_mask)
        else:
            grids_clone = self
        
        
        # create infection field
        infection_field = torch.zeros(
            grids_clone.batch_size, grids_clone.grid_size, grids_clone.grid_size, dtype=torch.float, device=grids_clone.device
        )
        
        statistics_buffers = {}
        if collect_statistics:
            # note stacking only for consistency of behaviour with the compute disease prop function. 
            # should be refactored later
            statistics_buffers = {
                "percentage_individuals_by_states": torch.stack([grids_clone.calculate_percentage_of_individuals_in_all_disease_states()], dim=2)
            }
            if collect_optimal_vaccines_count:
                statistics_buffers["optimal_vaccines_count"] = torch.stack([grids_clone.calculate_optimal_vaccines_count()], dim=2)
        
        
        susceptible_mask = grids_clone.grids[:, :, :, DiseaseState.SUSCEPTIBLE.value] == 1
        infectious_mask = grids_clone.grids[:, :, :, DiseaseState.INFECTIOUS.value] == 1
        recovered_mask = grids_clone.grids[:, :, :, DiseaseState.RECOVERED.value] == 1
        vaccinated_mask = grids_clone.grids[:, :, :, DiseaseState.VACCINATED.value] == 1
        
        
        # in the beginning set the infection field to 1 for infectious people
        infection_field[infectious_mask] = 1 
        
        ## Instantiate infection_field with         
        for _ in range(num_disease_steps):
            # Get infected neighbours mask
            
            """
            # Game plan            
            """            
            
            # Calculate the probability of each cell not transmitting the infection
            transmission_prob = 1 - infection_field * infection_probability
            
            # Perform convolution to get the product of (1 - p_j * I_p) for all neighbors
            transmission_prob_log = torch.log(transmission_prob + 10**-10)
            # padded_transmission_prob_log = F.pad(transmission_prob_log, (1, 1, 1, 1), mode='circular').unsqueeze(1)  # toric mode
            padded_transmission_prob_log = F.pad(transmission_prob_log, (1, 1, 1, 1), mode='constant', value=0).unsqueeze(1) # zero padding
            prod_transmission_prob_log = F.conv2d(padded_transmission_prob_log, self.infectious_neighbour_count_kernel).squeeze(1)
            # convert back from log space to prob space
            prod_transmission_prob = torch.exp(prod_transmission_prob_log)
            
            # Calculate the probability of not being infected from neighbors
            prob_not_infected_from_neighbors = prod_transmission_prob
            
            # Calculate the probability of staying infected considering recovery
            prob_staying_infected = infection_field * (1 - recovery_probability)
            
            # Update the infection probability for each cell                        
            infection_field = 1 - (1 - prob_staying_infected) * prob_not_infected_from_neighbors
                        
            infection_field[~susceptible_mask] = 0 # force non-susceptible people to have infection probability of 0 
                                                   # to ensure the disease does not propagate through them

            infection_field[infectious_mask] = 1 # force infectious people to have infection probability of 1
            # rethink if this plays well with the recovery probability
        
        # in the last step, mark infectious individuals as having an infection field of 0
        # as we are more interested in the new infections
        infection_field[infectious_mask] = 0
        
        # add entry to cache
        self.add_function_output_to_cache("get_infection_field", infection_field)
        
        return infection_field, grids_clone, statistics_buffers


    def calculate_immediate_infection_risk_mask(self):
        """
        Calculate the mask for the optimal number of vaccines assuming ring vaccination.

        Returns:
        torch.Tensor: A boolean mask indicating where vaccines should be applied.
                      Dimension: batch_size x grid_size x grid_size
        """
        # check for cache hit
        if self.check_for_cache_hit("calculate_immediate_infection_risk_mask"): return self.retrieve_from_cache("calculate_immediate_infection_risk_mask")
        
        
        infectious_neighbours_count = self.count_infected_neighbours()
        # Identify susceptible cells
        susceptible_mask = self.grids[:, :, :, DiseaseState.SUSCEPTIBLE.value] == 1
        # Identify cells with at least one infected neighbor
        infectious_neighbours_count_mask = infectious_neighbours_count >= 1
        # Combine masks to identify susceptible cells with infected neighbors
        immediate_infection_risk_mask = susceptible_mask * infectious_neighbours_count_mask
        
        # add entry to cache
        self.add_function_output_to_cache("calculate_immediate_infection_risk_mask", immediate_infection_risk_mask)
        
        return immediate_infection_risk_mask

    def calculate_optimal_vaccines_count(self):
        """
        Calculate the optimal number of vaccines needed.

        Returns:
        torch.Tensor: A tensor containing the count of optimal vaccines needed for each batch.
                      Dimension: batch_size x 1
        """
        # check for cache hit
        if self.check_for_cache_hit("calculate_optimal_vaccines_count"): return self.retrieve_from_cache("calculate_optimal_vaccines_count")
        
        immediate_infection_risk_mask = self.calculate_immediate_infection_risk_mask()
        # Sum the number of true values in the mask to get the count of vaccines needed
        optimal_vaccines_count = immediate_infection_risk_mask.sum(axis=(1, 2)).unsqueeze(1)
        
        # add entry to cache
        self.add_function_output_to_cache("calculate_optimal_vaccines_count", optimal_vaccines_count)
        
        return optimal_vaccines_count

    def calculate_percentage_of_individuals_in_all_disease_states(self, batch_subset_mask=None):
        """
        Calculate the percentage of individuals in each disease state.

        Returns:
        torch.Tensor: A tensor containing the percentage of individuals in each state for each batch.
                      Dimension: batch_size x num_disease_states x disease_sim_steps
        """
        
        if batch_subset_mask is None:
            batch_subset_mask = slice(None)
        
        # check for cache hit
        if self.check_for_cache_hit("calculate_percentage_of_individuals_in_all_disease_states"): return self.retrieve_from_cache("calculate_percentage_of_individuals_in_all_disease_states")

        grids = self.grids[batch_subset_mask]
        
        # Sum the number of individuals in each state
        num_individuals_by_states = grids.sum(dim=(1, 2), dtype=torch.float)
        # Calculate the population by subtracting empty cells from total cells
        population_by_batch = (self.grid_size * self.grid_size) - num_individuals_by_states[:, DiseaseState.EMPTY.value]
        population_by_batch = population_by_batch.unsqueeze(1) # Adjust shape for broadcasting
        # Calculate the percentage of individuals in each state
        percentage_individuals_by_states = num_individuals_by_states / population_by_batch
        # Set the percentage for empty cells to 0 as it's not meaningful
        percentage_individuals_by_states[:, DiseaseState.EMPTY.value] = 0.0
        
        # add entry to cache
        self.add_function_output_to_cache("calculate_percentage_of_individuals_in_all_disease_states", percentage_individuals_by_states)
        
        return percentage_individuals_by_states

    def visualize_grid(self, grids=None, batch_idx=0, highlight_cells=[], title_text=None, plt_show=True):
        """
        Visualize the grid for a specific batch.

        Parameters:
        grids (torch.Tensor): Grids to visualize. If None, use the internal grids.
        batch_idx (int): Index of the batch to visualize.
        highlight_cells (list): List of cells to highlight in the plot.
        title_text (str): Text to display as the title of the plot.
        plt_show (bool): Whether to show the plot.

        Returns:
        tuple: Figure and axis of the plot if plt_show is False.
        """
        if grids is None:
            grids = self.grids
        
        if isinstance(grids, type(self)):
            grids = self.grids
        
        _, GRID_SIZE, _, _ = grids.shape
        grid = grids[batch_idx].cpu().numpy()
        img = np.ones((GRID_SIZE, GRID_SIZE, 3), dtype=np.float32)

        # Define colors for each disease state
        state_colors = {
            DiseaseState.EMPTY:       [247, 245, 251],
            DiseaseState.SUSCEPTIBLE: [107, 191, 89],
            DiseaseState.INFECTIOUS:  [164, 36, 59],
            DiseaseState.RECOVERED:   [18, 38, 58],
            DiseaseState.VACCINATED:  [255, 136, 17],
        }
        # color palette from: https://coolors.co/ff8811-a4243b-12263a-067bc2-6bbf59

        # Color the grid based on the state of each cell
        for state, color in state_colors.items():
            img[grid[..., state.value] == 1] = np.array(color) / 255

        fig, ax = plt.subplots(figsize=(8, 8))
        ax.imshow(img)
        # Set minor ticks for grid lines
        ax.set_xticks(np.arange(-0.5, GRID_SIZE, 1), minor=True)
        ax.set_yticks(np.arange(-0.5, GRID_SIZE, 1), minor=True)
        ax.grid(which='minor', color='grey', linestyle=':', linewidth=0.25)
        ax.tick_params(which='both', left=False, bottom=False, labelleft=False, labelbottom=False)
        ax.set_frame_on(False)
        
        # Highlight specific cells if provided
        if highlight_cells:
            for cell in highlight_cells:
                rect = patches.Rectangle((cell[1] - 0.5, cell[0] - 0.5), 1, 1, linewidth=3, edgecolor='#067BC2', facecolor='none')
                ax.add_patch(rect)
        
        if title_text:
            plt.title(title_text, fontsize=8)

        # Create legend
        handles = [
            patches.Patch(color=np.array(state_colors[DiseaseState.SUSCEPTIBLE])/255, label='Susceptible'),
            patches.Patch(color=np.array(state_colors[DiseaseState.INFECTIOUS])/255, label='Infectious'),
            patches.Patch(color=np.array(state_colors[DiseaseState.RECOVERED])/255, label='Recovered'),
            patches.Patch(color=np.array(state_colors[DiseaseState.VACCINATED])/255, label='Vaccinated')
        ]
        ax.legend(handles=handles, loc='upper right', fontsize=8)

        if plt_show:
            plt.show()
        
        return fig, ax
    
    def get_egocentric_view(self, view_points):
        """
        Get the egocentric view of the grid from specified view points.
        This is useful when using the egocentric view in the observations for the gym environment.

        Parameters:
        view_points (torch.Tensor): Coordinates (y, x) of the view points.

        Returns:
        torch.Tensor: The egocentric view of the grids.
        """
        ys = view_points[:, 0]
        xs = view_points[:, 1]
        indices_y = torch.arange(self.grid_size, dtype=torch.int64, device=self.grids.device)
        indices_x = torch.arange(self.grid_size, dtype=torch.int64, device=self.grids.device)
        # Expand dimensions and repeat along the batch dimension
        batch_indices_y = indices_y.unsqueeze(0).expand(self.batch_size, -1)
        batch_indices_x = indices_x.unsqueeze(0).expand(self.batch_size, -1)
        # Calculate the relative indices
        relative_indices_y = (batch_indices_y - self.grid_size // 2 + ys.unsqueeze(1)) % self.grid_size
        relative_indices_x = (batch_indices_x - self.grid_size // 2 + xs.unsqueeze(1)) % self.grid_size
        # Expand the indices to match the dimensions of the grids tensor
        expanded_indices_x = relative_indices_x.unsqueeze(1).unsqueeze(3).expand(-1, self.grid_size, -1, len(DiseaseState))
        expanded_indices_y = relative_indices_y.unsqueeze(2).unsqueeze(3).expand(-1, -1, self.grid_size, len(DiseaseState))
        # Reorganize coordinates for final egocentric view
        egocentric_grids = self.grids.gather(1, expanded_indices_y).gather(2, expanded_indices_x)
        return egocentric_grids
    
    def clone_grid(self, subset_mask=None):
        """
        Clone the grid with an option to subset batches.

        Parameters:
        subset_mask (torch.Tensor): Indices of the batches to clone.

        Returns:
        torch.Tensor: Cloned grids tensor.
        """
        if subset_mask is None:
            return copy.deepcopy(self) # legacy way
        
        # if subset indices are provided, create a new instance and manually 
        # clone the relevant items + other housekeeping tasks
        
        grids_tensor_subset = self.grids[subset_mask]
        new_batch_size = grids_tensor_subset.size(0)
        
        grids_clone = DiseaseGrids(grids=grids_tensor_subset, batch_size=new_batch_size, grid_size=self.grid_size, device=self.device)
        grids_clone.invalidate_cache() # invalidate the whole function output cache
        
        return grids_clone

if __name__ == "__main__":
    # Define parameters
    batch_size = 4
    grid_size = 10
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
    # Create an instance of DiseaseGrids
    disease_grids = DiseaseGrids(grids=None, batch_size=batch_size, grid_size=grid_size, device=device)
    
    # Set some initial states (for demonstration purposes, we'll set random states)
    susceptible_mask = torch.rand(batch_size, grid_size, grid_size, device=device) > 0 # set all as susceptible
    infectious_mask = torch.rand(batch_size, grid_size, grid_size, device=device) < 0.01 # set 1% as infectious
    
    # Apply the states to the grids
    disease_grids.set_state(susceptible_mask, DiseaseState.SUSCEPTIBLE)
    disease_grids.set_state(infectious_mask, DiseaseState.INFECTIOUS)
    
    # Get infection field
    
    # Calculate optimal vaccines count
    optimal_vaccines_count = disease_grids.calculate_optimal_vaccines_count()
    print("Optimal Vaccines Count per Batch:", optimal_vaccines_count.cpu().numpy())
    
    # Calculate percentage of individuals in all disease states
    percentage_individuals_by_states = disease_grids.calculate_percentage_of_individuals_in_all_disease_states()
    print("Percentage of Individuals in Each State per Batch:", percentage_individuals_by_states.cpu().numpy())
    
    # Visualize one of the grids
    disease_grids.visualize_grid(batch_idx=0)
    
    # Get egocentric view for a specific point (for demonstration, we'll use the center point of the first grid)
    view_points = torch.tensor([[] for _ in range(batch_size)], device=device)
    view_points = torch.randint(low=0, high=disease_grids.grid_size, size=(batch_size, 2), device=device)
    
    egocentric_view = disease_grids.get_egocentric_view(view_points)
    print("Egocentric View for Center Point:", egocentric_view.cpu().numpy())
    
    
    # create a clone of the grid with just the first 3 batches
    grids_subset_clone = disease_grids.clone_grid(subset_mask=torch.arange(3))
    assert grids_subset_clone.grids.shape == (3, grid_size, grid_size, len(DiseaseState)), "Invalid grid shape for clone."
    
    
    # check infection field
    infection_field, future_grids, statistics_buffers = disease_grids.get_infection_field(
                infection_probability=0.5,
                num_disease_steps=4,
        collect_statistics=True,
        collect_optimal_vaccines_count=True
    )

    print("Infection field: ", infection_field)
    
    
