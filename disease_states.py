from enum import Enum


# Enum for disease states
class DiseaseState(Enum):
    EMPTY = 0
    SUSCEPTIBLE = 1
    INFECTIOUS = 2
    RECOVERED = 3
    VACCINATED = 4