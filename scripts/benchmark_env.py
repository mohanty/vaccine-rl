import cProfile
import io
import os.path
import pstats
import sys
from timeit import default_timer as timer

import pandas as pd
import torch
from loguru import logger

logger.remove()
logger.add(sys.stderr, level="INFO")

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from disease_env import DiseaseBatchEnv


def profile_environment_step(env, num_steps=400):
    """
    Profile the step function of the DiseaseBatchEnv using cProfile.

    Parameters:
    env (DiseaseBatchEnv): The environment to profile.
    num_steps (int): Number of steps to perform.

    Returns:
    pstats.Stats: Profiling statistics.
    """
    profiler = cProfile.Profile()
    profiler.enable()

    # Reset the environment and get the initial observations
    obs_batch = env.reset_batch()
    
    for _ in range(num_steps):
        # Sample actions for the batch
        actions = env.batch_env_action_space.sample()
        actions = torch.tensor(actions, device=device, dtype=torch.int32)

        env.step_batch(actions)
    
    profiler.disable()
    
    s = io.StringIO()
    ps = pstats.Stats(profiler, stream=s).sort_stats(pstats.SortKey.TIME)
    ps.print_stats()
    profiling_stats = s.getvalue()
    logger.info(profiling_stats)
    
    return ps

def benchmark_environment_step(env, num_steps=400, num_trials=5):
    """
    Benchmark the step function of the DiseaseBatchEnv.

    Parameters:
    env (DiseaseBatchEnv): The environment to benchmark.
    num_steps (int): Number of steps to perform in each trial.
    num_trials (int): Number of trials to run for benchmarking.

    Returns:
    pd.DataFrame: DataFrame containing the timing results.
    """
    times = []
    steps_covered = []
    throughput = []


    for trial in range(num_trials):
        start_time = timer()
                

        # Reset the environment and get the initial observations
        obs_batch = env.reset_batch()        
        
        for step in range(num_steps):
            # Sample actions for the batch
            actions = env.batch_env_action_space.sample()
            actions = torch.tensor(actions, device=device, dtype=torch.int32)
            
            env.step_batch(actions)
        
        end_time = timer()

        elapsed_time = end_time - start_time
        total_steps = num_steps * env.batch_size
        steps_per_second = total_steps / elapsed_time

        times.append(elapsed_time)
        steps_covered.append(total_steps)
        throughput.append(steps_per_second)

        logger.info(f"Trial {trial + 1}: {elapsed_time:.4f} seconds, Steps covered: {total_steps}, Throughput: {steps_per_second:.2f} steps/second")

    # Create a DataFrame for better visibility and analysis
    df = pd.DataFrame({
        'Elapsed Time': times,
        'Steps Covered': steps_covered,
        'Throughput (steps/second)': throughput
    })
    df['Trial'] = df.index + 1
    
    average_time = df['Elapsed Time'].mean()
    average_throughput = df['Throughput (steps/second)'].mean()
    
    logger.info(f"Average time over {num_trials} trials: {average_time:.4f} seconds")
    logger.info(f"Average throughput over {num_trials} trials: {average_throughput:.2f} steps/second")
    
    return df

if __name__ == "__main__":
    # Initialize the environment with example parameters
    device = "cuda" if torch.cuda.is_available() else "cpu"
    env = DiseaseBatchEnv(
        grid_size=100,
        batch_size=1000,
        initial_population_distribution={
            "susceptible": 0.90,
            "infectious": 0.02,
            "recovered": 0.02,
            "vaccinated": 0.02,
        },
        infection_probability=0.03,
        recovery_probability=0.02,
        vaccination_budget=None,
        skip_disease_simulation_for_non_vaccination_actions=True,
        infection_field_score_config = {
            "num_simulation_steps": 11,
        },
        disease_simulation_config = {
            "num_simulation_steps": 30,
        },
        rewards_composition_config = {
            "weight_cost_of_time": 1, # in steps 
            "weight_cost_of_valid_vaccination": 1, # in number of vaccines 
            "weight_cost_of_invalid_vaccination": 10, # in number of vaccines 
            "weight_reward_for_infection_field": 10.0, # mean infection field value
        },        
        max_timesteps=400,
        seed=None,
        device=device,
    )
        
    # Profile the environment step function
    profile_stats = profile_environment_step(env, num_steps=400)
    
    # Benchmark the environment step function
    benchmark_results = benchmark_environment_step(env, num_steps=400, num_trials=5)
    
    # Print benchmark results
    print("Benchmark Results:")
    print(benchmark_results)
    
    # Optionally save the benchmark results to a CSV file for further analysis
    benchmark_results.to_csv("benchmark_results.csv", index=False)
