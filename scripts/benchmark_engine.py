import cProfile
import io
import os.path
import pstats
import sys
from timeit import default_timer as timer

import pandas as pd
from loguru import logger

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))


from disease_engine import DiseaseEngine


def profile_disease_propagation(engine, num_disease_steps=100):
    """
    Profile the disease propagation step of the DiseaseEngine using cProfile.

    Parameters:
    engine (DiseaseEngine): The disease engine to profile.
    num_disease_steps (int): Number of disease propagation steps to perform.

    Returns:
    pstats.Stats: Profiling statistics.
    """
    profiler = cProfile.Profile()
    profiler.enable()
    engine.propagate_disease_step(num_disease_steps=num_disease_steps, collect_statistics=False)
    profiler.disable()
    
    s = io.StringIO()
    ps = pstats.Stats(profiler, stream=s).sort_stats(pstats.SortKey.TIME)
    ps.print_stats()
    profiling_stats = s.getvalue()
    logger.info(profiling_stats)
    
    return ps

def benchmark_disease_propagation(engine, num_disease_steps=100, num_trials=5):
    """
    Benchmark the disease propagation step of the DiseaseEngine.

    Parameters:
    engine (DiseaseEngine): The disease engine to benchmark.
    num_disease_steps (int): Number of disease propagation steps to perform in each trial.
    num_trials (int): Number of trials to run for benchmarking.

    Returns:
    pd.DataFrame: DataFrame containing the timing results.
    """
    times = []

    for _ in range(num_trials):
        start_time = timer()
        engine.propagate_disease_step(num_disease_steps=num_disease_steps, collect_statistics=False)
        end_time = timer()

        elapsed_time = end_time - start_time
        times.append(elapsed_time)
        logger.info(f"Trial {_ + 1}: {elapsed_time:.4f} seconds")

    # Create a DataFrame for better visibility and analysis
    df = pd.DataFrame(times, columns=['Elapsed Time'])
    df['Trial'] = df.index + 1
    
    average_time = df['Elapsed Time'].mean()
    logger.info(f"Average time over {num_trials} trials: {average_time:.4f} seconds")
    
    return df

if __name__ == "__main__":
    # Initialize the DiseaseEngine with default parameters
    engine = DiseaseEngine(grid_size=500, batch_size=1000)
    
    # Profile the disease propagation step
    profile_stats = profile_disease_propagation(engine, num_disease_steps=100)
    
    # Benchmark the disease propagation step
    benchmark_results = benchmark_disease_propagation(engine, num_disease_steps=100, num_trials=5)
    
    # Print benchmark results
    print("Benchmark Results:")
    print(benchmark_results)
    
    # Optionally save the benchmark results to a CSV file for further analysis
    benchmark_results.to_csv("benchmark_results.csv", index=False)
    
