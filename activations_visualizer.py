import io

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
from PIL import Image
from tensorboardX import SummaryWriter

from disease_env import DiseaseBatchEnv, DiseaseEnvStatisticsIndex


class ActivationVisualizer:
    def __init__(self, model, writer):
        self.model = model
        self.activations = {}
        self.hooks = []
        self.writer = writer
        
        self.register_hooks()

    def register_hooks(self):
        def get_activation(name):
            def hook(model, input, output):
                self.activations[name] = output.detach()
            return hook
        
        # Register hooks for each layer you want to visualize
        for name, layer in self.model.named_children():
            if isinstance(layer, nn.Sequential):  # For conv and fc layers
                for sublayer_name, sublayer in layer.named_children():
                    self.hooks.append(sublayer.register_forward_hook(get_activation(f'{name}_{sublayer_name}')))
            else:
                self.hooks.append(layer.register_forward_hook(get_activation(name)))

    def remove_hooks(self):
        for hook in self.hooks:
            hook.remove()
        self.hooks = []

    def visualize_activations(self, observation, infection_fields, vaccine_budgets, agent_positions, batch_idx=0, global_step=0):
        self.activations = {}  # Reset activations
        self.register_hooks()
        
        observation = observation[[batch_idx], :, : ]
        infection_fields = infection_fields[[batch_idx], :, :]
        vaccine_budgets = vaccine_budgets[[batch_idx]]
        agent_positions = agent_positions[[batch_idx], :]
        
        self.model.get_action_and_value((observation, infection_fields, vaccine_budgets, agent_positions))  # Forward pass to trigger hooks
        
        for name, activation in self.activations.items():
            if len(activation.shape) == 4:  # Conv layers
                num_filters = activation.shape[1]
                num_rows = int(np.sqrt(num_filters))
                num_cols = int(np.ceil(num_filters / num_rows))
                fig, axes = plt.subplots(num_rows, num_cols, figsize=(4, 4))
                fig.subplots_adjust(wspace=0.05, hspace=0.05)  
                if num_rows == 1 and num_cols == 1:
                    axes = np.array([axes])
                elif num_rows == 1 or num_cols == 1:
                    axes = axes.flatten()
                else:
                    axes = axes.flatten()
                
                for i in range(num_filters):
                    ax = axes[i]
                    ax.imshow(activation[0, i].cpu(), cmap='viridis')
                    ax.axis('off')
                for i in range(num_filters, len(axes)):
                    axes[i].axis('off')
                # plt.suptitle(f'Activations of {name}')
                # Save the figure to a temporary buffer and log it to TensorBoard
                buf = io.BytesIO()
                plt.savefig(buf, format='jpeg', bbox_inches='tight', pad_inches=0)
                buf.seek(0)
                image = np.array(Image.open(buf))
                image = np.transpose(image, (2, 0, 1))
                self.writer.add_image(f'activations/{name}', image, global_step)
                plt.close(fig)
            elif len(activation.shape) == 2:  # FC layers
                # Log the activations of fully connected layers as histograms
                self.writer.add_histogram(name, activation.cpu(), global_step)
        
        self.remove_hooks()

if __name__ == "__main__":
    from ppo import AgentConv as Agent

    # Example usage
    batch_envs = DiseaseBatchEnv(
        grid_size=5,
        batch_size=500,
        initial_population_distribution={
            "susceptible": 0.90,
            "infectious": 0.02,
            "recovered": 0.02,
            "vaccinated": 0.02,
        },
        infection_probability=1.0,
        recovery_probability=0.0,
        vaccination_budget=None,
        infection_impact_score_config = {
            "num_simulation_steps": 1,
            "num_simulation_steps_last_episode": 2,
            "discount_factor": 0.99,    
            "weight_susceptible": 1.0,
            "weight_infectious": 1.0,
            "weight_vaccinated": 1.0,
            "weight_vaccination_budget": 0.00, # not being used currently
            "penalty_for_invalid_vaccination": 0.01, # penalty when vaccinating non susceptible cells
        },        
        max_timesteps=400,
        seed=None,
        device="cuda",
    )

    writer = SummaryWriter(f"runs/test-run")

    model = Agent(batch_envs)

    visualizer = ActivationVisualizer(model, writer)

    # Create some dummy data for visualization
    dummy_observation = torch.zeros((1, *model.observation_shape))
    dummy_vaccine_budgets = torch.zeros(1)

    # Visualize activations
    visualizer.visualize_activations(dummy_observation, dummy_vaccine_budgets)
    visualizer.remove_hooks()  # Clean up hooks when done
    visualizer.writer.close()  # Close the TensorBoard writer when done
