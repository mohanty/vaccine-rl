# vaccine-RL 
reinforcement learning for vaccination strategies.

# setup
```
git clone git@gitlab.epfl.ch:mohanty/vaccine-rl.git
cd vaccine-rl

pip install -r requirements.txt
```

# usage
```python

from disease_engine import DiseaseEngine

device = "cuda" if torch.cuda.is_available() else "cpu"

engine = DiseaseEngine(
                    grid_size=15, 
                    batch_size=100, 
                    initial_population_fractions = None,
                    infection_probability=0.02, 
                    recovery_probability=0.01,
                    seed=19911706,
                    device=device)

engine = DiseaseEngine(grid_size=10, batch_size=4)

# Perform one step of disease propagation and collect statistics
updated_grids, statistics = engine.propagate_disease_step(num_disease_steps=1, collect_statistics=True)

# Print statistics
print("Statistics of Disease States:")
print(statistics)

# Visualize the first batch grid
engine.visualize_grid(batch_idx=0)

```


# benchmark engine performance
```bash

python scripts/benchmark_engine.py
```

# todo
- [ ] run disease simulation only for vaccinated agents 
- [ ] vectorize vaccine application on a batch - too much SPS perf variances is directly tied to the distribution of vaccine actions

# notes
- too large mini batch sizes mess up stability during training
- weight_infectious needs to be atleast 10 times weight_vaccination_budget to enable stable bootstrapping of exploration - for grid size 4 - can be analytically figured out
    - has to be figured out for other grid sizes to ensure the cost of a ring vaccination <= cost of the vaccines needed to do it 
- minibatch size should be around 1500 ish !

# author
sharada mohanty (mohanty@aicrowd.com)