from enum import Enum

import gymnasium as gym
import numpy as np
import torch
from loguru import logger
from torch_scatter import scatter_min

from disease_engine import DiseaseEngine
from disease_states import DiseaseState


# Enumeration for statistics indices
class DiseaseEnvStatisticsIndex(Enum):
    PERCENTAGE_SUSCEPTIBLE_POST_SIM = 0
    PERCENTAGE_INFECTIOUS_POST_SIM = 1
    PERCENTAGE_RECOVERED_POST_SIM = 2
    PERCENTAGE_VACCINATED_POST_SIM = 3
    
class InfectionFieldSimulationStatisticsIndex(Enum):
    SUM_INFECTION_FIELD = 0
    MEAN_INFECTION_FIELD_PER_SUSCEPTIBLE = 1
    DISTANCE_TO_INFECTION_CENTER_OF_MASS = 2
    NUM_SUSCEPTIBLE_EXPOSED = 3
    PERCENTAGE_SUSCEPTIBLE = 4
    PERCENTAGE_INFECTIOUS = 5
    PERCENTAGE_RECOVERED = 6
    PERCENTAGE_VACCINATED = 7
    
class DiseaseEnvRewardsCompositionIndex(Enum):
    POTENTIAL_DIFFERENCE_INFECTION_FIELD = 0
    POTENTIAL_DIFFERENCE_INFECTION_FIELD_DISTANCE = 1
    MEAN_COST_OF_INVALID_VACCINATION = 2
    MEAN_COST_OF_VALID_VACCINATION = 3
    MEAN_COST_OF_TIME = 4

# Enumeration for possible actions
class DiseaseEnvActions9(Enum):
    MOVE_NORTH_WEST = 0
    MOVE_NORTH = 1
    MOVE_NORTH_EAST = 2
    MOVE_EAST = 3
    MOVE_SOUTH_EAST = 4
    MOVE_SOUTH = 5
    MOVE_SOUTH_WEST = 6
    MOVE_WEST = 7
    VACCINATE = 8

class DiseaseEnvActions5(Enum):
    MOVE_NORTH = 0
    MOVE_EAST = 1
    MOVE_SOUTH = 2
    MOVE_WEST = 3
    VACCINATE = 4
    
class DiseaseEnvActions3(Enum):
    MOVE_NORTH = 0
    MOVE_EAST = 1
    VACCINATE = 2

DiseaseEnvActions = DiseaseEnvActions9 # Use the action space with just N, E, W, S + VACCINATE
# DiseaseEnvActions = DiseaseEnvActions5 # Use the action space with just N, E, W, S + VACCINATE
# DiseaseEnvActions = DiseaseEnvActions3 # Use the action space with just N, E, + VACCINATE (in a toric grid this is sufficient to navigate the grid)

# Main environment class
class DiseaseBatchEnv(gym.Env):
    def __init__(
        self,
        grid_size=5,
        batch_size=100,
        egocentric_observations=True,
        gamma=1,
        initial_population_distribution={ # can also specify this as absolute numbers wrt the grid size
            "susceptible": 24, 
            "infectious": 1, 
            "recovered": 0.00,
            "vaccinated": 0.00,
        },
        infection_probability=0.03,
        recovery_probability=0.02,
        infection_field_score_config = {
            "num_simulation_steps": 4,
        },
        disease_simulation_config = {
            "num_simulation_steps": 4,
        },
        rewards_composition_config = {
            "weight_cost_of_time": 1, # in steps 
            "weight_cost_of_valid_vaccination": 1, # in number of vaccines 
            "weight_cost_of_invalid_vaccination": 10, # in number of vaccines 
            "weight_reward_for_infection_field": 10.0, # mean infection field value
        },
        vaccination_budget=None,
        skip_disease_simulation_for_non_vaccination_actions=False, # lets you not run the disease simulation for non-vaccination actions
        max_timesteps=50,
        seed=None,
        device=None,
    ):

        # Initialize parameters
        self.grid_size = grid_size
        self.batch_size = batch_size
        self.egocentric_observations = egocentric_observations

        self.initial_population_distribution={
            DiseaseState.SUSCEPTIBLE: initial_population_distribution["susceptible"],
            DiseaseState.INFECTIOUS: initial_population_distribution["infectious"],
            DiseaseState.RECOVERED: initial_population_distribution["recovered"],
            DiseaseState.VACCINATED: initial_population_distribution["vaccinated"],
        }

        self.gamma = gamma # Discount factor for rewards in the environment 
        self.infection_probability = infection_probability
        self.recovery_probability = recovery_probability
        
        self.infection_field_score_config = infection_field_score_config
        self.disease_simulation_config = disease_simulation_config
        self.rewards_composition_config = rewards_composition_config
        
        self.max_timesteps = max_timesteps
        self.max_vaccination_budget = vaccination_budget or self.max_timesteps
        self.skip_disease_simulation_for_non_vaccination_actions = skip_disease_simulation_for_non_vaccination_actions
        self.seed = seed
        self.device = torch.device(
            device or ("cuda" if torch.cuda.is_available() else "cpu")
        )
        
        self.writer = None
        self.global_step = None

        # Initialize the disease engine and set up spaces
        self.initialize_disease_engine()
        self.set_observation_and_action_spaces()
        self.reset_state_tracking_variables()

    def initialize_disease_engine(self):
        """Initialize the disease engine with provided parameters."""
        self.disease_engine = DiseaseEngine(
            grid_size=self.grid_size,
            batch_size=self.batch_size,
            initial_population_distribution=self.initial_population_distribution,
            infection_probability=self.infection_probability,
            recovery_probability=self.recovery_probability,
            seed=self.seed,
            device=self.device,
        )

    def set_observation_and_action_spaces(self):
        """Define observation and action spaces for the environment."""
        # Observation space is a grid of size (grid_size, grid_size) with values scaled between 0 and 1
        self.single_env_observation_space = gym.spaces.Dict(
                                                {
                                                    "grids": gym.spaces.Box(
                                                        low=0, high=1, shape=(self.grid_size, self.grid_size)
                                                    ),
                                                    "infection_fields": gym.spaces.Box(
                                                        low=0, high=1, shape=(self.grid_size, self.grid_size)
                                                    ),
                                                    "vaccine_budgets": gym.spaces.Discrete(self.max_vaccination_budget + 1),
                                                    "agent_positions": gym.spaces.Box(
                                                        low=0, high=1, shape=(2,)
                                                    )
                                                }
                                            )
        # Action space is discrete with a number of actions equal to the length of DiseaseEnvActions
        self.single_env_action_space = gym.spaces.Discrete(
            len(DiseaseEnvActions)
        )
        
        # Batch action space is a box with a size equal to the batch size
        self.batch_env_action_space = gym.spaces.Box(
            low=0,
            high=len(DiseaseEnvActions) - 1,
            shape=(self.batch_size,),
            dtype=np.int8,
        )

    def reset_state_tracking_variables(self):
        """Reset all state tracking variables for a new simulation run."""
        self.current_step = -1  # Initial step

        # Initialize agent positions randomly within the grid
        self.batch_current_agent_positions = torch.randint(
            low=0,
            high=self.grid_size,
            size=(self.batch_size, 2),
            dtype=torch.int,
        ).to(self.device)
        
        ## Using fixed starting point for the agent
        # self.batch_current_agent_positions = torch.zeros( 
        #     (self.batch_size, 2), dtype=torch.int
        # ).to(self.device)

        # Map actions to coordinate changes
        try:
            foo = DiseaseEnvActions.MOVE_NORTH_EAST
            # DiseaseEnvActions9 is being used
            self.actions_coordinate_transform_map = torch.tensor(
                [
                    [-1, -1],
                    [-1, 0],
                    [-1, 1],
                    [0, 1],
                    [1, 1],
                    [1, 0],
                    [1, -1],
                    [0, -1],
                    [0, 0],
                ],
                device=self.device,
            )
        except AttributeError:
            # DiseaseEnvActions5 or DiseaseEnvAvtions3 are being used
            
            try:
                foo = DiseaseEnvActions.MOVE_SOUTH
                # DiseaseEnvActions5 is being used
                self.actions_coordinate_transform_map = torch.tensor(
                    [
                        [-1, 0],
                        [0, 1],
                        [1, 0],
                        [0, -1],
                        [0, 0],
                    ],
                    device=self.device,
                )
            except AttributeError:
                # DiseaseEnvActions3 is being used
                self.actions_coordinate_transform_map = torch.tensor(
                    [
                        [-1, 0],
                        [0, 1],
                        [0, 0],
                    ],
                    device=self.device,
                )
                        
                        
        # Initialize tensors for observations, actions, rewards, dones, and vaccination budgets
        # self.batch_grids = torch.zeros(
        #     [self.max_timesteps + 1]
        #     + list(self.disease_engine.grids.size()),
        #     device=self.device,
        # )
        # self.batch_actions = torch.zeros(
        #     (self.max_timesteps + 1, self.batch_size)
        #     + self.single_env_action_space.shape,
        #     device=self.device,
        # )
        
        self.batch_dones = torch.zeros(
            (self.max_timesteps, self.batch_size), dtype=torch.bool
        ).to(self.device)
        self.batch_rewards = torch.zeros(
            (self.max_timesteps, self.batch_size), dtype=torch.float
        ).to(self.device)
        self.batch_episode_lengths = torch.zeros(
                    (self.max_timesteps, self.batch_size), dtype=torch.float
                ).to(self.device)
                
        self.batch_vaccination_budget = (
            torch.ones(self.batch_size, dtype=torch.int).to(self.device)
            * self.max_vaccination_budget
        )
        
        self.batch_infection_fields = torch.zeros(
            (
                self.batch_size, self.grid_size, self.grid_size # NOTE: shape is batch_size, grid_size, grid_size unlike the rest
            ), # todo- fix me in refactor
            dtype=torch.float
        ).to(self.device)
        
        self.batch_num_valid_vaccinations = torch.zeros(
            (self.max_timesteps, self.batch_size), dtype=torch.int
        ).to(self.device)
        self.batch_num_invalid_vaccinations = torch.zeros(
            (self.max_timesteps, self.batch_size), dtype=torch.int
        ).to(self.device)
        
        self.batch_is_disease_simulation_needed = (
            torch.ones(self.batch_size, dtype=torch.bool).to(self.device)
        ) # instantiate every batch index as disease sim needed
                
        self.infection_field_simulation_statistics = torch.zeros(
            (
                self.max_timesteps + 1,
                len(InfectionFieldSimulationStatisticsIndex),
                self.batch_size,
            ),
            dtype=torch.float
        ).to(self.device)
        
        self.batch_disease_simulation_statistics = torch.zeros(
            (
                self.batch_size,
                len(DiseaseEnvStatisticsIndex),
            ),
            dtype=torch.float
        ).to(self.device) - 1 # -1 to indicate that the value is not yet computed
        
        self.rewards_composition_statistics = torch.zeros(
            (
                self.max_timesteps + 1,
                len(DiseaseEnvRewardsCompositionIndex),
                self.batch_size,
            ),
            dtype=torch.float
        ).to(self.device)
        
        
        self.episode_end_post_sim_grid_states = torch.zeros(
                    (self.batch_size, self.grid_size, self.grid_size, len(DiseaseState)), dtype=torch.float
                ).to(self.device)
        # holds grid stats at the end of disease sim after the episode is done

        # Calculate disease simulation metrics for the initial observation
        # self.calculate_disease_simulation_metrics() 
        # provide no batch subset, so its computed for the whole batch 
        # and the stats are stored at -1 index in the statistics tensor
        self.calculate_infection_field_metrics() 

    def get_current_observation_batch(self):
        """Get the current batch of observations with ego-centric views."""
        
        # if self.egocentric_observations: 
        #     grids = self.disease_engine.grids.get_egocentric_view(
        #         self.batch_current_agent_positions
        #     )
        #     batch_agent_y = torch.ones(self.batch_size, dtype=torch.int) * (self.grid_size // 2)
        #     batch_agent_x = torch.ones(self.batch_size, dtype=torch.int) * (self.grid_size // 2)            
        # else:
        # # non ego centric view
        #     grids = self.disease_engine.grids.grids
        #     batch_agent_y = self.batch_current_agent_positions[:, 0]
        #     batch_agent_x = self.batch_current_agent_positions[:, 1]
            
            
        # disable ego centric view for now
        # batch_agent_y = self.batch_current_agent_positions[:, 0]
        # batch_agent_x = self.batch_current_agent_positions[:, 1]
        
        
        grids = self.disease_engine.grids.grids
        grids = (grids.argmax(dim=-1) / (len(DiseaseState) - 1 )) # in range 0 - 1
        
        # observation = torch.zeros(
        #     (self.batch_size, 2, self.grid_size, self.grid_size),
        #     device=self.device,
        # )
        
        # batch_ids = torch.arange(self.batch_size, device=self.device)
        
        # # 3 channels
        # observation[:, 0, :, :] = grids # actual grid state in the first channel
        # # observation[batch_ids, 1].zero_() # initialize the agent position channel as 0
        # # observation[batch_ids, 1, batch_agent_y, batch_agent_x] = 1 # mark agent position in the second channel
        # observation[batch_ids, 1, :, :] = self.batch_infection_fields  # infection field in the third channel
                
        return {
            "grids": grids, 
            "infection_fields": self.batch_infection_fields,
            "vaccine_budgets": self.batch_vaccination_budget,
            "agent_positions": self.batch_current_agent_positions / self.grid_size, # normalize agent positions
        }
        
    def reset_batch(self):
        """Reset the entire batch of environments."""
        self.disease_engine.reset()
        self.reset_state_tracking_variables()
        return self.get_current_observation_batch()

    def _step_vaccinate_batch(self, batch_actions):
        """Vaccinate individuals in the batch based on actions."""
                
        # Create a mask to identify which actions are 'VACCINATE'
        batch_vaccination_mask = (
            batch_actions == DiseaseEnvActions.VACCINATE.value
        )
                
        # identify subset of the batch that are potential vaccination candidates
        vaccination_candidate_indices = torch.nonzero(batch_vaccination_mask).flatten()
        agent_y_at_vaccination_candidate_indices = self.batch_current_agent_positions[vaccination_candidate_indices, 0]
        agent_x_at_vaccination_candidate_indices = self.batch_current_agent_positions[vaccination_candidate_indices, 1]
        
        # Check if the vaccination actions are valid
        
        ## check if the cell the agent is in is susceptible (only on the subset of vaccination candidates)
        susceptible_cell_sub_mask = self.disease_engine.grids[vaccination_candidate_indices, agent_y_at_vaccination_candidate_indices, agent_x_at_vaccination_candidate_indices, DiseaseState.SUSCEPTIBLE.value] == 1
        ## check if the vaccination budget is available for the corresponding batches
        valid_vaccination_budget_sub_mask = self.batch_vaccination_budget[vaccination_candidate_indices] > 0
        
        valid_vaccination_candidate_sub_mask = torch.logical_and(susceptible_cell_sub_mask, valid_vaccination_budget_sub_mask)
        invalid_vaccination_candidate_sub_mask = ~valid_vaccination_candidate_sub_mask
        
        ## mark valid candidates as vaccinated
        valid_vaccination_candidate_mask = torch.zeros((self.batch_size,), dtype=torch.bool, device=self.device)
        valid_vaccination_candidate_mask[vaccination_candidate_indices[valid_vaccination_candidate_sub_mask]] = True
        
        agent_y_coords = self.batch_current_agent_positions[valid_vaccination_candidate_mask, 0]
        agent_x_coords = self.batch_current_agent_positions[valid_vaccination_candidate_mask, 1]
        self.disease_engine.grids[valid_vaccination_candidate_mask, agent_y_coords, agent_x_coords] = self.disease_engine.grids.disease_state_vectors[DiseaseState.VACCINATED]
        # invalidate the disease engine grid cache after the manual set operation
        self.disease_engine.grids.invalidate_cache() 
        
        ## mark invalid candidates for penalty
        invalid_vaccination_candidate_mask = torch.zeros((self.batch_size,), dtype=torch.bool, device=self.device)
        invalid_vaccination_candidate_mask[vaccination_candidate_indices[invalid_vaccination_candidate_sub_mask]] = True
        
        
        ## reduce vaccination budget for all vaccination actions
        self.batch_vaccination_budget -= batch_vaccination_mask.to(torch.int)
        self.batch_vaccination_budget = torch.clamp(self.batch_vaccination_budget, min=0)
        
        ## update the number of valid and invalid vaccinations
        self.batch_num_valid_vaccinations[self.current_step] += valid_vaccination_candidate_mask
        self.batch_num_invalid_vaccinations[self.current_step] += invalid_vaccination_candidate_mask
        
        return valid_vaccination_candidate_mask, invalid_vaccination_candidate_mask
    
    def calculate_infection_field_metrics(self, num_simulation_steps=None, batch_subset_mask=None, register_statistics=True):        
        if batch_subset_mask is None:
            batch_subset_mask = torch.ones(self.batch_size, dtype=torch.bool, device=self.device)
        
        if num_simulation_steps is None:
            num_simulation_steps = self.infection_field_score_config["num_simulation_steps"]

        batch_vaccination_budget = self.batch_vaccination_budget[batch_subset_mask]
        
        # Compute infection field
        infection_field, future_grids, statistics = self.disease_engine.grids.get_infection_field(
            infection_probability=self.infection_probability, recovery_probability=self.recovery_probability,
            batch_subset_mask=batch_subset_mask, num_disease_steps=num_simulation_steps,
            collect_statistics=True, collect_optimal_vaccines_count=False
        )
        
        sum_infection_field = infection_field.sum(dim=(1, 2))
        
        num_susceptible = statistics[
            "percentage_individuals_by_states"
        ][:, DiseaseState.SUSCEPTIBLE.value, 0] * self.grid_size * self.grid_size
        mean_infection_field_per_susceptible = sum_infection_field / num_susceptible
        num_susceptible_exposed = (infection_field > 0).sum(dim=(1,2))

        
        # calculate distance to the center of the infection field
        sub_batch_size, rows, cols = infection_field.shape
        # Create coordinate grids for rows and columns
        x_coords = torch.arange(cols, device=self.device).repeat(rows, 1)
        y_coords = torch.arange(rows, device=self.device).repeat(cols, 1).t()
                
        # Expand the coordinate grids to match the batch size
        x_coords = x_coords.unsqueeze(0).expand(sub_batch_size, -1, -1)
        y_coords = y_coords.unsqueeze(0).expand(sub_batch_size, -1, -1)

        # Calculate the weighted sum of coordinates
        weighted_sum_x = (x_coords * infection_field).sum(dim=(1, 2))
        weighted_sum_y = (y_coords * infection_field).sum(dim=(1, 2))
        
        total_weight = infection_field.sum(dim=(1, 2))
        
        # Calculate the center of the infection field for each item in the batch
        center_x = weighted_sum_x / total_weight
        center_y = weighted_sum_y / total_weight

        # for cases where the infection field has already been anulled by the agent
        # set the center of mass as the agent position
        cases_with_no_infection_field = total_weight == 0
        center_x[cases_with_no_infection_field] = self.batch_current_agent_positions[batch_subset_mask, 1][cases_with_no_infection_field].to(torch.float)
        center_y[cases_with_no_infection_field] = self.batch_current_agent_positions[batch_subset_mask, 0][cases_with_no_infection_field].to(torch.float)


        # Stack the centers to get the final result
        # centers = torch.stack((center_x, center_y), dim=1)
        
        dx = torch.abs(self.batch_current_agent_positions[batch_subset_mask, 1] - center_x)
        dy = torch.abs(self.batch_current_agent_positions[batch_subset_mask, 0] - center_y)
        
        # dx = torch.min(dx, self.grid_size - dx) # only needed for toric grids
        # dy = torch.min(dy, self.grid_size - dy)
        # calculate distance to the center of the infection field        
        distance_to_infection_center_of_mass = (dx + dy) 
        
        
        percentage_susceptible = statistics[
            "percentage_individuals_by_states"
        ][:, DiseaseState.SUSCEPTIBLE.value, 0]
        percentage_infectious = statistics["percentage_individuals_by_states"][
            :, DiseaseState.INFECTIOUS.value, 0
        ]
        percentage_recovered = statistics["percentage_individuals_by_states"][
            :, DiseaseState.RECOVERED.value, 0
        ]
        percentage_vaccinated = statistics["percentage_individuals_by_states"][
            :, DiseaseState.VACCINATED.value, 0
        ]
        
        # save latest infection fields for batch
        self.batch_infection_fields[batch_subset_mask] = infection_field
                
        if register_statistics:
            self.register_infection_field_simulation_statistics(
                torch.stack([
                    sum_infection_field,
                    mean_infection_field_per_susceptible,
                    distance_to_infection_center_of_mass,
                    num_susceptible_exposed,
                    percentage_susceptible,
                    percentage_infectious,
                    percentage_recovered,
                    percentage_vaccinated,
                ]),
                batch_subset_mask=batch_subset_mask
            )
        return {
            "sum_infection_field": sum_infection_field,
            "mean_infection_field_per_susceptible": mean_infection_field_per_susceptible,
            "distance_to_infection_center_of_mass": distance_to_infection_center_of_mass,
            "num_susceptible_exposed": num_susceptible_exposed,
            "percentage_susceptible": percentage_susceptible,
            "percentage_infectious": percentage_infectious,
            "percentage_recovered": percentage_recovered,
            "percentage_vaccinated": percentage_vaccinated,
        }

    def register_infection_field_simulation_statistics(self, infection_field_sim_statistics, batch_subset_mask):
        """Store simulation statistics."""

        # store provided values in the statistics tensor for the batch subset 
        batch_subset_indices = torch.nonzero(batch_subset_mask).flatten()
        self.infection_field_simulation_statistics[self.current_step, :, batch_subset_indices] = infection_field_sim_statistics
        
        # for the batch indices not in the subset, copy over the values from the previous step
        not_batch_subset_indices = torch.nonzero(~batch_subset_mask).flatten()
        self.infection_field_simulation_statistics[self.current_step, :, not_batch_subset_indices] = self.infection_field_simulation_statistics[self.current_step -1, :, not_batch_subset_indices]
                
        

    def calculate_disease_simulation_metrics(self, num_simulation_steps=None, batch_subset_mask=None, batch_vaccination_budget=None, register_statistics=True):
        """Calculate various metrics related to disease simulation.
        """
                    
        if batch_subset_mask is None:
            # batch_subset_mask = slice(None) # stand in for : slicing
            batch_subset_mask = torch.ones(self.batch_size, dtype=torch.bool, device=self.device)
                    
        if batch_vaccination_budget is None:
            batch_vaccination_budget = self.batch_vaccination_budget[batch_subset_mask]

        # Simulation parameters        
        if num_simulation_steps is None:
            # override the num simulation steps for metrics calculation in last time step of episode
            num_simulation_steps = self.disease_simulation_config["num_simulation_steps"]        

        # Simulate future disease progression and collect statistics
        future_grids, statistics = self.disease_engine.propagate_disease_step(
            batch_subset_mask=batch_subset_mask, num_disease_steps=num_simulation_steps, collect_statistics=True, collect_optimal_vaccines_count=True, collect_immediate_infection_risk_mask=False
        )
        
        assert statistics["percentage_individuals_by_states"].size(0) == batch_subset_mask.sum() # check if the statistics tensor has the same batch size as the mask

        # Extract percentage of individuals in different states

        percentage_susceptible = statistics[
            "percentage_individuals_by_states"
        ][:, DiseaseState.SUSCEPTIBLE.value, :]
        percentage_infectious = statistics["percentage_individuals_by_states"][
            :, DiseaseState.INFECTIOUS.value, :
        ]
        percentage_recovered = statistics["percentage_individuals_by_states"][
            :, DiseaseState.RECOVERED.value, :
        ]
        percentage_vaccinated = statistics["percentage_individuals_by_states"][
            :, DiseaseState.VACCINATED.value, :
        ]                                        

        # Register the calculated statistics
        if register_statistics: 
            self.register_disease_simulation_statistics(
                torch.stack([
                    percentage_susceptible[:, -1],
                    percentage_infectious[:, -1],
                    percentage_recovered[:, -1],
                    percentage_vaccinated[:, -1],
                ]).T, # note - the ordering is important
                batch_subset_mask=batch_subset_mask
            )

        return {                
                "percentage_susceptible" : percentage_susceptible,
                "percentage_infectious" : percentage_infectious,
                "percentage_recovered" : percentage_recovered,
                "percentage_vaccinated" : percentage_vaccinated,
                "future_grids": future_grids
            }        

    def register_disease_simulation_statistics(self, disease_sim_statistics, batch_subset_mask):
        """Store simulation statistics."""

        # store provided values in the statistics tensor for the batch subset 
        batch_subset_indices = torch.nonzero(batch_subset_mask).flatten()
        self.batch_disease_simulation_statistics[batch_subset_mask, :] = disease_sim_statistics
                    
        

    def step_batch(self, batch_actions):
        """Perform a step in the environment for the entire batch."""
        self.current_step += 1  # Increment the current step
        
        if self.current_step >= self.max_timesteps:
            # todo: also check on the done function
            raise Exception("Calling step_batch after the environment is done or max timesteps reached.")        

        # Handle vaccination actions
        valid_vaccinations_mask, invalid_vaccinations_mask = self._step_vaccinate_batch(batch_actions)

        # Compute new agent positions based on actions
        batch_agent_positions_delta = self.actions_coordinate_transform_map[
            batch_actions
        ]
        # self.batch_current_agent_positions = (
        #     self.batch_current_agent_positions + batch_agent_positions_delta
        # ) % self.grid_size
        self.batch_current_agent_positions = torch.clamp( 
            self.batch_current_agent_positions + batch_agent_positions_delta, min=0, max=self.grid_size - 1
        )

        # Calculate disease simulation metrics for valid batch indices
        ## observation - disease simulation only needs to be done for 
        ##               batch indices where the action was a valid vaccination
        if self.skip_disease_simulation_for_non_vaccination_actions:
            self.batch_is_disease_simulation_needed = torch.logical_or(valid_vaccinations_mask, self.batch_is_disease_simulation_needed)
        else:
            self.batch_is_disease_simulation_needed[:] = 1
                            
        infection_field_simulation_results = self.calculate_infection_field_metrics(
            num_simulation_steps=self.infection_field_score_config["num_simulation_steps"],
            batch_subset_mask=self.batch_is_disease_simulation_needed,
            register_statistics=True
        )
        
        # mark steps where the disease simulation is not needed
        self.batch_is_disease_simulation_needed[self.batch_is_disease_simulation_needed] = 0
        
        
        # Environments are done if no susceptible individuals left or vaccine budget is exhausted
        # or infection field is completely nullified
        dones = self.check_dones(invalid_vaccinations_mask)
        self.batch_dones[self.current_step] = dones
        
        
        # Calculate rewards
        rewards = self.calculate_rewards(valid_vaccinations_mask, invalid_vaccinations_mask)
        
        if self.current_step > 1: # skip the first step
            rewards_done_mask = (~self.batch_dones[self.current_step - 1]) # mask rewards for done (in previous step) environments
            rewards = rewards * rewards_done_mask
                    
        self.batch_rewards[self.current_step] = rewards # add rewards to internal buffer for statistics calculation

        # Collect latest observation
        observation_batch = self.get_current_observation_batch()
        
        
        # prepare per-step info object
        info = {
            "current_step": self.current_step,
            "batch_rewards": self.batch_rewards,
            "batch_dones": self.batch_dones,
            "vaccination_budget_left": self.batch_vaccination_budget,
            "episode_lengths": self.batch_episode_lengths,
            "batch_valid_vaccinations": self.batch_num_valid_vaccinations,
            "batch_invalid_vaccinations": self.batch_num_invalid_vaccinations,
            "batch_disease_simulation_statistics": self.batch_disease_simulation_statistics,
            "infection_field_simulation_statistics": self.infection_field_simulation_statistics[self.current_step],
            "rewards_composition_statistics": self.rewards_composition_statistics[self.current_step],
            "num_disease_simulation_steps": self.infection_field_score_config["num_simulation_steps"],
            "episode_end_post_sim_grid_states": self.episode_end_post_sim_grid_states
        }

        # run last step simulation for items which are done for the first time
                
        # for items that are (newly) done in this step 
        # run the end of episode simulations
        batch_subset_that_is_done_in_this_step = torch.logical_and(dones, ~self.batch_dones[self.current_step - 1])
        done_subset_indices = torch.nonzero(batch_subset_that_is_done_in_this_step).flatten()
        info["done_subset_indices"] = done_subset_indices
        
        if len(done_subset_indices) > 0:
            end_of_episode_simulation_results = self.calculate_disease_simulation_metrics(
                num_simulation_steps=self.disease_simulation_config["num_simulation_steps"],
                batch_subset_mask=batch_subset_that_is_done_in_this_step,
                register_statistics=True)
            info["num_disease_simulation_steps"] = self.disease_simulation_config["num_simulation_steps"]

            # add future grids to of end of simulaion grids            
            self.episode_end_post_sim_grid_states[done_subset_indices] = end_of_episode_simulation_results["future_grids"].grids

            # add rewards to internal buffer for statistics calculation
            self.batch_rewards[self.current_step] = rewards 
                        
        return observation_batch, rewards, dones, info
        
    def state_potential_infection_field_sum(self, timestep):
        """
        Potential function to calculate the value of a state wrt infection field at a given time step.
        """
        infection_field_sum = self.infection_field_simulation_statistics[
            timestep,
            InfectionFieldSimulationStatisticsIndex.SUM_INFECTION_FIELD.value,
            :,
        ]
        mean_infection_field_per_susceptible = self.infection_field_simulation_statistics[
            timestep,
            InfectionFieldSimulationStatisticsIndex.MEAN_INFECTION_FIELD_PER_SUSCEPTIBLE.value,
            :,
        ]
        
        # todo - add abs distance to infection field center of mass ?
        # print(infection_field_sum)
        # return (self.grid_size * self.grid_size) - infection_field_sum # range  [0 , grid_size]
        return  - mean_infection_field_per_susceptible * self.grid_size * self.grid_size
    
    def state_potential_infection_field_distance(self, timestep):
        distance_to_infection_center_of_mass = self.infection_field_simulation_statistics[ 
            timestep,
            InfectionFieldSimulationStatisticsIndex.DISTANCE_TO_INFECTION_CENTER_OF_MASS.value,
            :
        ]
        # normalized_distance_to_infection_center_of_mass = distance_to_infection_center_of_mass / self.grid_size
        # print(normalized_distance_to_infection_center_of_mass, distance_to_infection_center_of_mass)
        # return 1 - normalized_distance_to_infection_center_of_mass
        
        return -1 * distance_to_infection_center_of_mass
    
    def calculate_rewards(self, valid_vaccinations_mask, invalid_vaccinations_mask):
        """
        Calculate the rewards for the current step based on multiple factors, including lives saved,
        costs of vaccinations, cost of time, and infection risk proximity.

        Args:
        - valid_vaccinations_mask (torch.Tensor): Mask indicating valid vaccinations actions across the whole batch
        - invalid_vaccinations_mask (torch.Tensor): Mask indicating invalid vaccinations acrions across the whole batch 

        Returns:
        - rewards (torch.Tensor): The computed rewards for the given step for the whole batch.
        """
        
        potential_discount_factor = self.gamma
        # potential_discount_factor = 1
        
        # Infection Field Potential
        infection_field_previous_potential = self.state_potential_infection_field_sum(self.current_step - 1)
        infection_field_current_potential = self.state_potential_infection_field_sum(self.current_step)
        
        potential_difference_infection_field = (potential_discount_factor * infection_field_current_potential) - infection_field_previous_potential
        potential_difference_infection_field *= self.rewards_composition_config["weight_reward_for_infection_field"]
        
        # print("Field: ", potential_difference_infection_field)
        
        # Infection Field CoM Distance 
        infection_field_previous_potential_distance = self.state_potential_infection_field_distance(self.current_step - 1)
        infection_field_current_potential_distance = self.state_potential_infection_field_distance(self.current_step)
        
        potential_difference_infection_field_distance = (potential_discount_factor * infection_field_current_potential_distance) - infection_field_previous_potential_distance
        potential_difference_infection_field_distance *= self.rewards_composition_config["weight_reward_for_infection_field_distance"]
        
        # print(infection_field_current_potential_distance, infection_field_previous_potential_distance)
        # print("Distance: ", potential_difference_infection_field_distance)
        
        # Cost of Vaccinations
        # Add a penalty for invalid vaccinations.
        cost_of_invalid_vaccination = (
            invalid_vaccinations_mask * self.rewards_composition_config["weight_cost_of_invalid_vaccination"]
        ).to(torch.float)

        # Add a cost for each valid vaccination action.
        cost_of_valid_vaccination = (
            valid_vaccinations_mask * self.rewards_composition_config["weight_cost_of_valid_vaccination"]
        ).to(torch.float)

        # Cost of Time
        # Add a cost for each action taken.
        cost_of_time = torch.ones(self.batch_size, dtype=torch.bool, device=self.device) * self.rewards_composition_config["weight_cost_of_time"]

        # Total Reward
        # Combine all rewards and costs to get the total reward.
        rewards = (
            potential_difference_infection_field
            + potential_difference_infection_field_distance
            - cost_of_invalid_vaccination
            - cost_of_valid_vaccination
            - cost_of_time
        )
        if torch.any(rewards.isnan()):
            print("nan rewards indices: ", torch.nonzero(rewards.isnan()))
            print("nan rewards: ", rewards[rewards.isnan()])
        
        self.rewards_composition_statistics[self.current_step] = torch.stack([
            potential_difference_infection_field,
            potential_difference_infection_field_distance,
            cost_of_invalid_vaccination,
            cost_of_valid_vaccination,
            cost_of_time,
        ]) # note - order is important

        return rewards
        
    def check_dones(self, invalid_vaccinations_mask):
        """Check if any of the environments are done."""
        percentage_susceptible = self.get_percentage_susceptible(self.current_step)
        vaccine_budget_exhausted = self.batch_vaccination_budget == 0
        no_susceptible_left = percentage_susceptible == 0
        no_infection_field_left = self.batch_infection_fields.sum(axis=(1,2)) == 0
        
        
        
        # Environments are done if no susceptible individuals left or vaccine budget is exhausted 
        # or no infection field is left
        dones = torch.logical_or(no_susceptible_left, vaccine_budget_exhausted)
        dones = torch.logical_or(dones, no_infection_field_left)
        # dones = torch.logical_or(dones, invalid_vaccinations_mask) # stop if invalid vaccination is done
        
        # Ensure environments are done if max timesteps reached
        if self.current_step >= self.max_timesteps - 1:
            dones.fill_(True)
        
        # increase episode length for all environments that are not done
        self.batch_episode_lengths[self.current_step] += ~dones
        
        
        return dones

    def render(self, grids=None, batch_idx=0, egocentric=False, plt_show=True):
        """Render the grid for a specific batch index."""
        view_points = None
        title_text = (
            f"batch {batch_idx} | step {self.current_step} | ego: {egocentric} | "
            f"vaccine_budget: {self.batch_vaccination_budget[batch_idx]} | "
            f"agent_pos: {self.batch_current_agent_positions[batch_idx].tolist()}"
        )
        

        if egocentric:
            view_points = self.batch_current_agent_positions
            highlight_cells = [(self.grid_size // 2, self.grid_size // 2)]
        else:
            highlight_cells = [
                self.batch_current_agent_positions[batch_idx].tolist()
            ]

        return self.disease_engine.visualize_grid(
            grids=grids,
            batch_idx=batch_idx,
            view_points=view_points,
            highlight_cells=highlight_cells,
            title_text=title_text,
            plt_show=plt_show,
        )

    def get_statistic(self, statistic_index, timestep):
        """
        Retrieve a specific statistic for a given timestep for the whole batch.

        Args:
            statistic_index (DiseaseEnvStatisticsIndex): The index of the statistic to retrieve.
            timestep (int): The timestep at which to retrieve the statistic.

        Returns:
            torch.Tensor: The values of the requested statistic for the whole batch.
        """
        return self.infection_field_simulation_statistics[timestep, statistic_index.value, :]
    
    def get_percentage_susceptible(self, timestep):
        """
        Retrieve the percentage of susceptible individuals for a given timestep for the whole batch.

        Args:
            timestep (int): The timestep at which to retrieve the statistic.

        Returns:
            torch.Tensor: The percentage of susceptible individuals for the whole batch.
        """
        return self.get_statistic(InfectionFieldSimulationStatisticsIndex.PERCENTAGE_SUSCEPTIBLE, timestep)
    
    def get_percentage_infectious(self, timestep):
        """
        Retrieve the percentage of infectious individuals for a given timestep for the whole batch.

        Args:
            timestep (int): The timestep at which to retrieve the statistic.

        Returns:
            torch.Tensor: The percentage of infectious individuals for the whole batch.
        """
        return self.get_statistic(InfectionFieldSimulationStatisticsIndex.PERCENTAGE_INFECTIOUS, timestep)

    def get_percentage_recovered(self, timestep):
        """
        Retrieve the percentage of recovered individuals for a given timestep for the whole batch.

        Args:
            timestep (int): The timestep at which to retrieve the statistic.

        Returns:
            torch.Tensor: The percentage of recovered individuals for the whole batch.
        """
        return self.get_statistic(InfectionFieldSimulationStatisticsIndex.PERCENTAGE_RECOVERED, timestep)
    
    def get_percentage_vaccinated(self, timestep):
        """
        Retrieve the percentage of vaccinated individuals for a given timestep for the whole batch.

        Args:
            timestep (int): The timestep at which to retrieve the statistic.

        Returns:
            torch.Tensor: The percentage of vaccinated individuals for the whole batch.
        """
        return self.get_statistic(InfectionFieldSimulationStatisticsIndex.PERCENTAGE_VACCINATED, timestep)
    



if __name__ == "__main__":
    import sys
    import time

    import torch
    from loguru import logger

    logger.remove()
    logger.add(sys.stderr, level="INFO")

    # Initialize the environment and perform a step
    device = "cuda" if torch.cuda.is_available() else "cpu"
    # device = "cpu"
    
    batch_envs = DiseaseBatchEnv(
        grid_size=10,
        batch_size=1100,
        initial_population_distribution={
            "susceptible": 99,
            "infectious": 1,
            "recovered": 0,
            "vaccinated": 0,
        },
        infection_probability=0.5,
        recovery_probability=0.0,
        vaccination_budget=5,
        infection_field_score_config = {
            "num_simulation_steps": 4,
        },
        disease_simulation_config = {
            "num_simulation_steps": 4,
        },
        rewards_composition_config = {
            "weight_cost_of_time": 1, # in steps 
            "weight_cost_of_valid_vaccination": 1, # in number of vaccines 
            "weight_cost_of_invalid_vaccination": 10, # in number of vaccines 
            "weight_reward_for_infection_field": 10.0, # mean infection field value
            "weight_reward_for_infection_field_distance": 1.0, # distance to infection field center of mass
        },
        max_timesteps=200,
        seed=None,
        device=device,
    )

    # Reset the environment and get the initial observations
    obs_batch = batch_envs.reset_batch()

    # Sample actions for the batch and perform a step
    actions = batch_envs.batch_env_action_space.sample()
    actions = torch.tensor(actions, device=device, dtype=torch.int32)
    actions[0] = DiseaseEnvActions.VACCINATE.value

    # Perform a step with the sampled actions
    batch_envs.step_batch(actions)

    # Retrieve some statistics for the whole batch at timestep 0

    timestep = 0
    
    # batch_0_infected_location = torch.nonzero(batch_envs.disease_engine.grids.grids[0, :, :, DiseaseState.INFECTIOUS.value] == 1)[0]
    
    # for k in range(10):
    #     batch_0_agent_location = batch_envs.batch_current_agent_positions[0]
    #     logger.info(f"Agent location: {batch_0_agent_location} | Infected location: {batch_0_infected_location}")
    #     actions[0] = DiseaseEnvActions.MOVE_SOUTH.value
    #     batch_envs.step_batch(actions)
    #     logger.info("Moved South")
        
    #     batch_0_agent_location = batch_envs.batch_current_agent_positions[0]        
    #     logger.info(f"Agent location: {batch_0_agent_location} | Infected location: {batch_0_infected_location}")
    #     actions[0] = DiseaseEnvActions.MOVE_EAST.value
    #     batch_envs.step_batch(actions)
    #     logger.info("Moved east")
        
    
    
    
    
    # logger.info(f"Percentage Susceptible: {batch_envs.get_percentage_susceptible(timestep)}")
    # logger.info(f"Percentage Infectious: {batch_envs.get_percentage_infectious(timestep)}")
    # logger.info(f"Percentage Recovered: {batch_envs.get_percentage_recovered(timestep)}")
    # logger.info(f"Percentage Vaccinated: {batch_envs.get_percentage_vaccinated(timestep)}")
    # logger.info(f"Infection Impact Score: {batch_envs.get_infection_impact_score(timestep)}")
    # logger.info(f"Discounted Infection Impact Score: {batch_envs.get_discounted_infection_impact_score(timestep)}")

    ### RL Loop ###
    num_episodes = 1
    for episode in range(num_episodes):
        # Record the start time of the episode
        start_time = time.time()

        rewards, dones, info = None, None, None
        # Reset the environment at the start of each episode
        obs_batch = batch_envs.reset_batch()
        
        total_rewards = torch.zeros(batch_envs.batch_size, device=device)
        done_batch = torch.zeros(batch_envs.batch_size, dtype=torch.bool, device=device)
        steps_per_env = torch.zeros(batch_envs.batch_size, dtype=torch.int32, device=device)  # Initialize step counter per environment
        
        current_step = 0
        
        while not done_batch.all():            
            # Sample random actions for the batch
            actions = batch_envs.batch_env_action_space.sample()
            actions = torch.tensor(actions, device=device, dtype=torch.int32)
            
            
            # Perform a step with the sampled actions
            obs_batch, rewards, dones, info = batch_envs.step_batch(actions)
                        
            # logger.info(f"Info: {info}")
            # print(rewards)
            # print(dones)
            # Accumulate rewards
            total_rewards += rewards * (~done_batch).float()
            
            # Update done statuses and steps per environment
            steps_per_env += (~done_batch).int()  # Increment steps only for environments that are not done
            done_batch |= dones  # Update done statuses
            
                    
        # Record the end time of the episode
        end_time = time.time()
        episode_duration = end_time - start_time
        
        logger.info(f"Episode {episode + 1} finished with mean rewards: {total_rewards.mean()}")
        logger.info(f"Episode {episode + 1} took {episode_duration:.2f} seconds.")
        logger.info(f"Total steps environment: {steps_per_env.sum()}")
        logger.info(f"Info: {info}")

    logger.info("RL loop completed.")
