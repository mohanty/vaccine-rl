# docs and experiment results can be found at https://docs.cleanrl.dev/rl-algorithms/ppo/#ppo_atari_envpoolpy
import os
import random
import sys
import time
from collections import OrderedDict, deque
from dataclasses import dataclass, field
from enum import Enum

import gymnasium as gym
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import tqdm
import tyro
from loguru import logger

# from torch.utils.tensorboard import SummaryWriter
from tensorboardX import SummaryWriter
from torch.distributions.categorical import Categorical

from activations_visualizer import ActivationVisualizer
from disease_engine import DiseaseEngine
from disease_env import (
    DiseaseBatchEnv,
    DiseaseEnvActions,
    DiseaseEnvRewardsCompositionIndex,
    DiseaseEnvStatisticsIndex,
    InfectionFieldSimulationStatisticsIndex,
)
from disease_states import DiseaseState
from utils import CategoricalMasked

# torch.autograd.set_detect_anomaly(True)

logger.remove()
logger.add(sys.stderr, level="INFO")


@dataclass
class Args:
    exp_name: str = os.path.basename(__file__)[: -len(".py")]
    """the name of this experiment"""
    seed: int = 2
    """seed of the experiment"""
    torch_deterministic: bool = True
    """if toggled, `torch.backends.cudnn.deterministic=False`"""
    cuda: bool = True
    """if toggled, cuda will be enabled by default"""
    track: bool = False
    """if toggled, this experiment will be tracked with Weights and Biases"""
    wandb_project_name: str = "cleanRL"
    """the wandb's project name"""
    wandb_entity: str = None
    """the entity (team) of wandb's project"""
    capture_video: bool = False
    """whether to capture videos of the agent performances (check out `videos` folder)"""

    # Algorithm specific arguments
    env_id: str = "DiseaseEnv-Vectorized"
    """the id of the environment"""
    total_timesteps: int = 10**8
    """total timesteps of the experiments"""
    learning_rate: float = 2e-4
    """the learning rate of the optimizer"""
    anneal_lr: bool = True
    """Toggle learning rate annealing for policy and value networks"""
    gae_lambda: float = 0.95
    """the lambda for the general advantage estimation"""
    update_epochs: int = 10
    """the K epochs to update the policy"""
    norm_adv: bool = True
    """Toggles advantages normalization"""
    clip_coef: float = 0.2
    """the surrogate clipping coefficient"""
    clip_vloss: bool = True
    """Toggles whether or not to use a clipped loss for the value function, as per the paper."""
    ent_coef: float = 0.01 
    """coefficient of the entropy"""
    vf_coef: float = 0.5
    """coefficient of the value function"""
    max_grad_norm: float = 0.5
    """the maximum norm for the gradient clipping"""
    # target_kl: float = None
    """the target KL divergence threshold"""
    target_kl: float = None
    """the target KL divergence threshold"""
    gamma: float = 0.10
    """the discount factor gamma"""
        
    log_stats_every_n_iterations: dict = field(default_factory=lambda: {
      "grid_visualizations": 10, 
      "model_weights_visualizations": 10,
      "rewards_distribution": 10,
      "actions_distribution": 10,
      "gradients_distribution": 10,
    })
    """"tensorboard viz frequency """
        
    minibatch_size: int = 128
    """size of each minibatch when training the policy"""
    num_steps: int = 50 # max_manhattan_distance = grid_size / 2 || so 2 * max_manhattan_distance + vaccination_budget
    """the number of steps to run in each environment per policy rollout"""    
    
    
    epsilon: float = 0.05 
    """implements an episolon greedy exploration strategy"""
    
    epsilon_greedy_action_sampling_weights: list = field(default_factory=lambda: [0, 0, 0, 0, 0, 0, 0, 0, 1])
    """the weights for different actions when using epsilon greedy action sampling - the indices should be from DiseaseEnvActions"""
    """careful when using different action sets"""
    
    disable_epsilon_greedy_after_n_steps: int = -1 # never stop epsilon greedy action sampling
    

    # disease sim specific configurations
    num_envs: int = 8192
    """the number of parallel environments"""
    
    grid_size: int = 6    
    """the size of the grid"""
    egocentric_observations: bool = False
    """if to use egocentric observations"""
    initial_population_distribution: dict = field(default_factory=lambda: {
        "susceptible": 35, # grid_size * grid_size - 1, 
        "infectious": 1, 
        "recovered": 0.00,
        "vaccinated": 0.00,
    })
    """the initial population fractions"""
    infection_probability: float = 0.125  
    """the probability of infection"""
    recovery_probability: float = 0.0
    """the probability of recovery"""
    vaccination_budget: float = 16 # 3* required for optimal vaccination + change
    """the vaccination budget"""
    skip_disease_simulation_for_non_vaccination_actions: bool = False
    infection_field_score_config: dict = field(default_factory=lambda: {
        "num_simulation_steps": 5, # same as grid size ?
    })
    disease_simulation_config: dict = field(default_factory=lambda: {
        "num_simulation_steps": 10,
    })
    rewards_composition_config: dict = field(default_factory=lambda: {
        "weight_cost_of_time": 0.01, # in steps 
        "weight_cost_of_valid_vaccination": 0, # in number of vaccines 
        "weight_cost_of_invalid_vaccination": 0, # in number of vaccines 
        "weight_reward_for_infection_field": 0.1, # mean infection field value - range [0 - 1]
        "weight_reward_for_infection_field_distance": 0 # range [-1, 1]
    })
    
    infection_field_smoothing_factor: float = 1.1 # x^a smoothing factor for infection field
        
    # non_vaccination_action_subsampling_rate = 0.001
    """what fractions of sar samples with negative rewards to use for training"""
    
    ensure_num_vaccine_actions_is_equal_to_num_non_vaccine_actions = False


    # to be filled in runtime
    batch_size: int = 0
    """the batch size (computed in runtime)"""
    # minibatch_size: int = 0
    """the mini-batch size (computed in runtime)"""
    num_iterations: int = 0
    """the number of iterations (computed in runtime)"""
    

def layer_init(layer, std=np.sqrt(2), bias_const=0.0):
    torch.nn.init.orthogonal_(layer.weight, std)
    torch.nn.init.constant_(layer.bias, bias_const)
    return layer

class LambdaLayer(nn.Module):
    def __init__(self, func):
        super(LambdaLayer, self).__init__()
        self.func = func

    def forward(self, x):
        return self.func(x)


class AgentMLP(nn.Module):
    def __init__(self, envs):
        super().__init__()
        
        # Get the input shape from the environment observation space
        self.flatten_size = int(np.prod(envs.single_env_observation_space["grids"].shape)) + 1 # adding 1 for vaccine budget

        # Initialize fully connected layers
        self.fc_layers = self._init_fc_layers(self.flatten_size)
        
        # Actor and Critic layers
        self.actor = layer_init(nn.Linear(64, envs.single_env_action_space.n), std=0.01)
        self.critic = layer_init(nn.Linear(64, 1), std=1)
        
        self._print_num_parameters()
        
    def _init_fc_layers(self, flatten_size):
        """Initialize fully connected layers to replace convolutional layers."""
        return nn.Sequential(OrderedDict({
            'fc1': layer_init(nn.Linear(self.flatten_size, 32)),
            'Tanh1': nn.Tanh(),
            'fc2': layer_init(nn.Linear(32, 32)),
            'Tanh2': nn.Tanh(),
            
        }))

    def _print_num_parameters(self):
        """Print the number of parameters in the model."""
        total_params = sum(p.numel() for p in self.parameters())
        print(f"Total number of parameters: {total_params}")        

    def get_value(self, x):
        """Compute the value (critic) of the given state."""
        x = self._process_input(x)
        return self.critic(x)

    def get_action_and_value(self, x, action=None):
        """Compute the action and value of the given state."""
        x = self._process_input(x)
        logits = self.actor(x)
        probs = Categorical(logits=logits)
        if action is None:
            action = probs.sample()
        return action, probs.log_prob(action), probs.entropy(), self.critic(x)

    def _process_input(self, x):
        """Preprocess the input tensor by flattening it and applying fully connected layers."""
        observation, vaccine_budgets = x # unpack the tuple
        
        x = torch.cat([observation.view(observation.size(0), -1), vaccine_budgets.unsqueeze(1)], dim=-1)  # Flatten and concatenate
        x = self.fc_layers(x)
        return x

class AgentConv(nn.Module):
    def __init__(self, envs):
        super().__init__()
        
        # keep a copy of the env for reference
        self.envs = envs
        
        # Get the input shape from the environment observation space
        self.observation_shape = envs.single_env_observation_space["infection_fields"].shape
        self.flatten_size = int(np.prod(envs.single_env_observation_space["infection_fields"].shape)) + 1 + 2 # adding 1 for vaccine budget and 2 for agent positions

        # Initialize convolutional and fully connected layers
        self.conv_layers = self._init_conv_layers()
        self.fc_layers = self._init_fc_layers()
        
        # Actor and Critic layers
        # self.actor = layer_init(nn.Linear(8, envs.single_env_action_space.n), std=0.01)
        # self.critic = layer_init(nn.Linear(64, 1), std=1)
        
        self.actor = nn.Sequential(OrderedDict({
            'fc1': layer_init(nn.Linear(32, 32), std=1),
            'Tanh1': nn.Tanh(),
            'fc2': layer_init(nn.Linear(32, envs.single_env_action_space.n), std=0.01),
        }))
        
        self.critic = nn.Sequential(OrderedDict({
            'fc1': layer_init(nn.Linear(32, 32)),
            'Tanh1': nn.Tanh(),
            'fc2': layer_init(nn.Linear(32, 1), std=1),
        }))
        
        self.setup_movement_action_mask()
        
        self._print_num_parameters()
        
    def _init_conv_layers(self):
        """Initialize convolutional layers."""
        return nn.Sequential(OrderedDict({
            # 'padding': LambdaLayer(lambda x: F.pad(x, (1, 1, 1, 1), mode='circular')), # remove in non toric mode
            # 'padding': LambdaLayer(lambda x: F.pad(x, (1, 1, 1, 1), mode='constant', value=0)),
            'conv1': layer_init(nn.Conv2d(1, 16, kernel_size=3, stride=1, padding=0)),  
            'Tanh1': nn.Tanh(),
            'conv2': layer_init(nn.Conv2d(16, 4, kernel_size=3, stride=1, padding=0)), 
            'Tanh2': nn.Tanh(),
            # 'conv3': layer_init(nn.Conv2d(16, 16, kernel_size=2, stride=2, padding=0)),  # Smaller kernel size
            # 'Tanh3': nn.Tanh(),            
            'flatten': nn.Flatten()
        }))

    def _init_fc_layers(self):
        """Initialize fully connected layer."""
        conv_output_size = self._get_conv_output_size()
        return nn.Sequential(OrderedDict({
            'fc1': layer_init(nn.Linear(conv_output_size + 1 + 2 , 32)), # adding 1 for vaccine budget and 2 for agent positions
            'Tanh1': nn.Tanh(),
            'fc2': layer_init(nn.Linear(32, 32)),
            'Tanh2': nn.Tanh(),
        }))

    def _get_conv_output_size(self):
        """Calculate the size of the output from the convolutional layers."""
        with torch.no_grad():
            dummy_input = torch.zeros(1, 1, *self.observation_shape)
            conv_output = self.conv_layers(dummy_input)
            return conv_output.size(1)

    def _print_num_parameters(self):
        """Print the number of parameters in the model."""
        total_params = sum(p.numel() for p in self.parameters())
        print(f"Total number of parameters: {total_params}")

    def get_value(self, x):
        """Compute the value (critic) of the given state."""
        x = self._process_input(x)
        return self.critic(x)

    def setup_movement_action_mask(self):
        grid_size = self.observation_shape[1]
        self.action_mask = torch.ones(
            grid_size, grid_size, self.envs.single_env_action_space.n, dtype=torch.bool
        ).to(device)
        # cannot move north if Y=0
        self.action_mask[0, :, DiseaseEnvActions.MOVE_NORTH.value] = False
        # cannot move south if Y=grid_size-1
        self.action_mask[-1, :, DiseaseEnvActions.MOVE_SOUTH.value] = False
        # cannot move west if X=0
        self.action_mask[:, 0, DiseaseEnvActions.MOVE_WEST.value] = False
        # cannot move east if X=grid_size-1
        self.action_mask[:, -1, DiseaseEnvActions.MOVE_EAST.value] = False
        
        try:
            # try to set masks for DiseaseEnvActions9 - if they exist
            
            # cannot move north east if Y=0 or X=grid_size-1
            self.action_mask[0, -1, DiseaseEnvActions.MOVE_NORTH_EAST.value] = False
            # cannot move south east if Y=grid_size-1 or X=grid_size-1
            self.action_mask[-1, -1, DiseaseEnvActions.MOVE_SOUTH_EAST.value] = False
            # cannot move south west if Y=grid_size-1 or X=0
            self.action_mask[-1, 0, DiseaseEnvActions.MOVE_SOUTH_WEST.value] = False
            # cannot move north west if Y=0 or X=0
            self.action_mask[0, 0, DiseaseEnvActions.MOVE_NORTH_WEST.value] = False
        except ValueError:
            # excepted error when DiseaseEnvActions9 is not being used
            pass
        
    
    def prepare_batch_action_mask(self, x_raw):
        """Mask out invalid actions by setting their logits to -inf."""        
        # Mask movement related logits 
        # identify the already vaccinated cells
        susceptible_cells_mask = x_raw[0] == DiseaseState.SUSCEPTIBLE.value / (len(DiseaseState) - 1)
        
        grid_size = self.observation_shape[1]
        # get agent positions
        
        agent_positions_y_coords = (x_raw[3][:, 0] * grid_size).to(torch.long)
        agent_positions_x_coords = (x_raw[3][:, 1] * grid_size).to(torch.long)
        agent_positions_batch_indices = torch.arange(agent_positions_y_coords.size(0), device=device)
        
        # gather the base mask for restricted movement
        batch_action_mask = self.action_mask[agent_positions_y_coords, agent_positions_x_coords].clone()
        # mask out the vaccination action for batch indices where the agent is already on a vaccinated cell
        agent_cell_is_not_susceptible = susceptible_cells_mask[agent_positions_batch_indices, agent_positions_y_coords, agent_positions_x_coords] == False
        batch_action_mask[
            agent_cell_is_not_susceptible,
            DiseaseEnvActions.VACCINATE.value
        ] = False
                
        return batch_action_mask
    
    def get_action_and_value(self, x_raw, action=None, disable_action_masking=False):
        """Compute the action and value of the given state."""
        x = self._process_input(x_raw)
        
        raw_logits = self.actor(x)
        
        # Apply action mask if applicable
        if disable_action_masking:
            batch_action_mask = None
            # useful when manually forcing some actions to be Vaccination
            # and you still want the logits for downstream calculations
        else:
            batch_action_mask = self.prepare_batch_action_mask(x_raw)
        
        probs = CategoricalMasked(logits=raw_logits, mask=batch_action_mask)
        if action is None:
            action = probs.sample()
        return action, probs.log_prob(action), probs.entropy(), self.critic(x), raw_logits

    def _process_input(self, x):
        """Preprocess the input tensor by applying convolutional and fully connected layers."""
        observation, infection_fields, vaccine_budgets, agent_positions = x # unpack the tuple
        
        observation = observation.unsqueeze(1) # add 1 channel
        infection_fields = infection_fields.unsqueeze(1) # add 1 channel

        # check if observation has nan
        if torch.isnan(observation).any():
            print("observation has nan")
            print(observation)
        
        # smooth infection field values
        infection_fields = torch.pow(infection_fields, args.infection_field_smoothing_factor)
                
        conv_output = self.conv_layers(infection_fields)
        
        conv_output = conv_output.view(conv_output.size(0), -1) # Flatten conv output

        x = torch.cat([conv_output, vaccine_budgets.unsqueeze(1), agent_positions], dim=-1)  # Concatenate vaccine budgets
        x = self.fc_layers(x)
        return x

# Agent = AgentMLP
Agent = AgentConv

# batch_envs = DiseaseBatchEnv(
#     grid_size=50,
#     batch_size=500,
#     initial_population_distribution={
#         "susceptible": 0.90,
#         "infectious": 0.02,
#         "recovered": 0.02,
#         "vaccinated": 0.02,
#     },
#     infection_probability=1.0,
#     recovery_probability=0.0,
#     vaccination_budget=None,
#     infection_field_score_config = {
#         "num_simulation_steps": 4,
#     },
#     disease_simulation_config = {
#         "num_simulation_steps": 4,
#     },
#     rewards_composition_config = {
#         "weight_cost_of_time": 1, # in steps 
#         "weight_cost_of_valid_vaccination": 1, # in number of vaccines 
#         "weight_cost_of_invalid_vaccination": 10, # in number of vaccines 
#         "weight_reward_for_infection_field": 10.0, # mean infection field value
#     },    
#     max_timesteps=400,
#     seed=None,
#     device="cuda",
# )


# obs_raw = batch_envs.reset_batch() # this is already on cuda

# observation = torch.Tensor(obs_raw["grids"]).to("cuda:2")
# vaccine_budgets = torch.Tensor(obs_raw["vaccine_budgets"]).to("cuda:2")
# agent = Agent(batch_envs).to("cuda:2")

# action, logprob, _, value = agent.get_action_and_value((observation, vaccine_budgets))
# print(action.shape)

# exit(0)

if __name__ == "__main__":
    args = tyro.cli(Args)
    args.batch_size = int(args.num_envs * args.num_steps)
    # args.minibatch_size = int(args.batch_size // args.num_minibatches)
    args.num_iterations = args.total_timesteps // args.batch_size
    run_name = f"{args.env_id}__{args.exp_name}__{args.seed}__{int(time.time())}"
    if args.track:
        import wandb

        wandb.init(
            project=args.wandb_project_name,
            entity=args.wandb_entity,
            sync_tensorboard=True,
            config=vars(args),
            name=run_name,
            monitor_gym=True,
            save_code=True,
        )
    writer = SummaryWriter(f"runs/{run_name}")
    writer.add_text(
        "hyperparameters",
        "|param|value|\n|-|-|\n%s" % ("\n".join([f"|{key}|{value}|" for key, value in vars(args).items()])),
    )

    # TRY NOT TO MODIFY: seeding
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.backends.cudnn.deterministic = args.torch_deterministic

    device = torch.device("cuda" if torch.cuda.is_available() and args.cuda else "cpu")

    # # env setup
    batch_envs = DiseaseBatchEnv(
        grid_size=args.grid_size,
        batch_size=args.num_envs,
        egocentric_observations=args.egocentric_observations,
        initial_population_distribution=args.initial_population_distribution,
        infection_probability=args.infection_probability,
        recovery_probability=args.recovery_probability,
        vaccination_budget=args.vaccination_budget,
        skip_disease_simulation_for_non_vaccination_actions=args.skip_disease_simulation_for_non_vaccination_actions,
        infection_field_score_config = args.infection_field_score_config,
        disease_simulation_config = args.disease_simulation_config,
        rewards_composition_config = args.rewards_composition_config,
        max_timesteps=args.num_steps,
        seed=args.seed,
        device=device,
    )
    
    batch_envs.writer = writer
    
    agent = Agent(batch_envs).to(device)
    optimizer = optim.Adam(agent.parameters(), lr=args.learning_rate, eps=1e-5)
    
    
    activations_visualizer = ActivationVisualizer(agent, writer)

    # ALGO Logic: Storage setup
    
    obs = torch.zeros((args.num_steps, args.num_envs) + batch_envs.single_env_observation_space["grids"].shape).to(device)
    vaccine_budgets = torch.zeros((args.num_steps, args.num_envs)).to(device)
    infection_fields = torch.zeros((args.num_steps, args.num_envs) + batch_envs.single_env_observation_space["infection_fields"].shape).to(device)
    agent_positions = torch.zeros((args.num_steps, args.num_envs, 2)).to(device)
    
    actions = torch.zeros((args.num_steps, args.num_envs) + batch_envs.single_env_action_space.shape).to(device)
    logprobs = torch.zeros((args.num_steps, args.num_envs)).to(device)
    rewards = torch.zeros((args.num_steps, args.num_envs)).to(device)
    dones = torch.zeros((args.num_steps, args.num_envs)).to(device)
    values = torch.zeros((args.num_steps, args.num_envs)).to(device)
    avg_returns = deque(maxlen=20)

    # TRY NOT TO MODIFY: start the game
    global_step = 0
    effective_learning_step = 0
    start_time = time.time()
    
    batch_envs.global_step = global_step

    for iteration in tqdm.tqdm(range(1, args.num_iterations + 1)):
        # collect observation into appropraite torch tensors
        batch_obs_raw = batch_envs.reset_batch()
        next_obs = torch.Tensor(batch_obs_raw["grids"]).to(device)
        next_infection_fields = torch.Tensor(batch_obs_raw["infection_fields"]).to(device)
        next_vaccine_budgets = torch.Tensor(batch_obs_raw["vaccine_budgets"]).to(device)
        next_agent_positions = torch.Tensor(batch_obs_raw["agent_positions"]).to(device)
        
        episode_end_post_sim_grid_states = None # only needed for visualization - so inefficient at the moment - dont use unless you know what you are doing
        
        next_done = torch.zeros(args.num_envs).to(device)
        episode_num_epsilon_greedy_actions = torch.zeros(args.num_envs).to(device)
        
        if iteration % args.log_stats_every_n_iterations["grid_visualizations"] == 1:
            # plot episode beginning visualization to tensorboard for batch_idx=0
            logger.info("plotting episode begin grid visualization to tensorboard")
            figure, _ = batch_envs.render()
            
            # visualize model weights if applicable
            if iteration % args.log_stats_every_n_iterations["model_weights_visualizations"] == 1:
                visualization_step = global_step
                writer.add_figure("visualizations/beginning_of_episoide", figure, visualization_step)
            
            # plot begin activations to tensorboard
            activations_visualizer.visualize_activations(next_obs, next_infection_fields, next_vaccine_budgets, next_agent_positions, global_step = visualization_step)


        # Annealing the rate if instructed to do so.
        if args.anneal_lr:
            frac = 1.0 - (iteration - 1.0) / args.num_iterations
            lrnow = frac * args.learning_rate
            optimizer.param_groups[0]["lr"] = lrnow

        for step in range(0, args.num_steps):
            obs[step] = next_obs
            vaccine_budgets[step] = next_vaccine_budgets
            infection_fields[step] = next_infection_fields
            agent_positions[step] = next_agent_positions
            
            dones[step] = next_done

            # ALGO LOGIC: action logic
            with torch.no_grad():
                action, logprob, _, value, raw_logits = agent.get_action_and_value((next_obs, next_infection_fields, next_vaccine_budgets, next_agent_positions))
                # note - raw logits above are unmasked
                
                if args.epsilon > 0 and (args.disable_epsilon_greedy_after_n_steps == -1 or global_step < args.disable_epsilon_greedy_after_n_steps):
                    # calculate the epsilon greedy mask to choose envs where we choose a random action
                    epsilon_greedy_mask = torch.rand(next_obs.shape[0], device=device) < args.epsilon
                    num_epsilon_greedy_actions = epsilon_greedy_mask.sum()
                    
                    # generate new random actions for the epsilon greedy mask
                    updated_actions = torch.multinomial(
                        torch.tensor(args.epsilon_greedy_action_sampling_weights, dtype=torch.float, device=device),
                        num_samples=num_epsilon_greedy_actions,
                        replacement=True
                    )
                    corresponding_logits = raw_logits[epsilon_greedy_mask]  
                    corresponding_logprobs = Categorical(logits=corresponding_logits).log_prob(updated_actions)
                    
                    # overwritte the actions and logprobs for the epsilon greedy mask
                    action[epsilon_greedy_mask] = updated_actions
                    logprob[epsilon_greedy_mask] = corresponding_logprobs
                    
                    # add counter of epsilon greedy actions
                    episode_num_epsilon_greedy_actions[epsilon_greedy_mask] += 1
                    
                
                
                values[step] = value.flatten()
            actions[step] = action
            logprobs[step] = logprob

            # TRY NOT TO MODIFY: execute the game and log data.
            # next_obs, reward, next_done, info = batch_envs.step(action.cpu().numpy())
            batch_obs_raw, reward, next_done, info = batch_envs.step_batch(action) # action is already on gpu 
            rewards[step] = reward.view(-1) # reward already on gpu
            
            next_obs = batch_obs_raw["grids"] # already on correct device
            next_infection_fields = batch_obs_raw["infection_fields"] # already on correct device
            next_vaccine_budgets = torch.Tensor(batch_obs_raw["vaccine_budgets"]).to(device)
            next_agent_positions = torch.Tensor(batch_obs_raw["agent_positions"]).to(device)
            
            next_done = torch.Tensor(next_done).to(torch.int)
            
            # update global steps            
            global_step += (~info["batch_dones"][step]).sum() + len(info["done_subset_indices"]) 
            batch_envs.global_step = global_step
            
            
            # stor reference to episode_end_post_sim_grid_states
            episode_end_post_sim_grid_states = info["episode_end_post_sim_grid_states"]
            # plot stats for episodes that are done
            # if len(info["done_subset_indices"])  > 0:

            #     # todo: records metrics per episode 
            
            if torch.all(next_done):
                # end of this episode for the whole batch
                # done_subset_indices = info["done_subset_indices"]
                done_subset_indices = torch.arange(args.num_envs, device=device)
                    
                # when all episodes are done    
                writer.add_scalar("charts/mean_cumulative_episode_rewards", info["batch_rewards"][:, done_subset_indices].sum(axis=0).mean() , global_step)
                writer.add_scalar("charts/mean_episode_length", (~info["batch_dones"][:, done_subset_indices]).sum(dim=0).mean(dtype=torch.float) + 1, global_step)
                writer.add_scalar("charts/mean_vaccination_budget_left", info["vaccination_budget_left"][done_subset_indices].mean(dtype=torch.float), global_step)
                
                writer.add_scalar("charts/mean_episode_num_epsilon_greedy_actions", episode_num_epsilon_greedy_actions.mean(dtype=torch.float), global_step)
                                
                # population distributions            
                writer.add_scalar("charts/mean_susceptible_fraction", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.PERCENTAGE_SUSCEPTIBLE.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/mean_infectious_fraction", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.PERCENTAGE_INFECTIOUS.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/mean_recovered_fraction", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.PERCENTAGE_RECOVERED.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/mean_vaccinated_fraction", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.PERCENTAGE_VACCINATED.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/sum_infection_field", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.SUM_INFECTION_FIELD.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/mean_infection_field_per_susceptible", info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.MEAN_INFECTION_FIELD_PER_SUSCEPTIBLE.value, done_subset_indices].mean(), global_step)
                writer.add_scalar("charts/percentage_is_infection_contained", (info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.SUM_INFECTION_FIELD.value, done_subset_indices] == 0).to(torch.float).mean(), global_step)
                writer.add_scalar("charts/num_susceptible_exposed", (info["infection_field_simulation_statistics"][InfectionFieldSimulationStatisticsIndex.NUM_SUSCEPTIBLE_EXPOSED.value, done_subset_indices]).mean(), global_step)
                
                


                writer.add_scalar("charts/mean_susceptible_post_sim_fraction", info["batch_disease_simulation_statistics"][done_subset_indices, DiseaseEnvStatisticsIndex.PERCENTAGE_SUSCEPTIBLE_POST_SIM.value].mean(), global_step)
                writer.add_scalar("charts/mean_infectious_post_sim_fraction", info["batch_disease_simulation_statistics"][done_subset_indices, DiseaseEnvStatisticsIndex.PERCENTAGE_INFECTIOUS_POST_SIM.value].mean(), global_step)
                writer.add_scalar("charts/mean_recovered_post_sim_fraction", info["batch_disease_simulation_statistics"][done_subset_indices, DiseaseEnvStatisticsIndex.PERCENTAGE_RECOVERED_POST_SIM.value].mean(), global_step)
                writer.add_scalar("charts/mean_vaccinated_post_sim_fraction", info["batch_disease_simulation_statistics"][done_subset_indices, DiseaseEnvStatisticsIndex.PERCENTAGE_VACCINATED_POST_SIM.value].mean(), global_step)

                                
                # valid/invalid vaccination metrics
                writer.add_scalar("charts/mean_valid_vaccinations", info["batch_valid_vaccinations"][:, done_subset_indices].sum(dim=0).mean(dtype=torch.float), global_step)
                writer.add_scalar("charts/mean_invalid_vaccinations", info["batch_invalid_vaccinations"][:, done_subset_indices].sum(dim=0).mean(dtype=torch.float), global_step)
                                
                # add rewards compositions
                writer.add_histogram("rewards/potential_difference_infection_field", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.POTENTIAL_DIFFERENCE_INFECTION_FIELD.value , done_subset_indices].flatten(), global_step)
                writer.add_histogram("rewards/potential_difference_infection_field_distance", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.POTENTIAL_DIFFERENCE_INFECTION_FIELD_DISTANCE.value , done_subset_indices].flatten(), global_step)
                writer.add_histogram("rewards/mean_cost_of_valid_vaccination", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_VALID_VACCINATION.value , done_subset_indices].flatten(), global_step)
                writer.add_histogram("rewards/mean_cost_of_invalid_vaccination", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_INVALID_VACCINATION.value , done_subset_indices].flatten(), global_step)
                writer.add_histogram("rewards/mean_cost_of_time", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_TIME.value , done_subset_indices].flatten(), global_step)
                
                writer.add_scalar("rewards/chart-potential_difference_infection_field", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.POTENTIAL_DIFFERENCE_INFECTION_FIELD.value , done_subset_indices].mean(), global_step)
                writer.add_scalar("rewards/chart-potential_difference_infection_field_distance", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.POTENTIAL_DIFFERENCE_INFECTION_FIELD_DISTANCE.value , done_subset_indices].mean(), global_step)
                writer.add_scalar("rewards/chart-mean_cost_of_valid_vaccination", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_VALID_VACCINATION.value , done_subset_indices].mean(), global_step)
                writer.add_scalar("rewards/chart-mean_cost_of_invalid_vaccination", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_INVALID_VACCINATION.value , done_subset_indices].mean(), global_step)
                writer.add_scalar("rewards/chart-mean_cost_of_time", info["rewards_composition_statistics"][ DiseaseEnvRewardsCompositionIndex.MEAN_COST_OF_TIME.value , done_subset_indices].mean(), global_step)
                
                
        # log stats every n steps
        if iteration % args.log_stats_every_n_iterations["grid_visualizations"] == 1:
            # plot episode end visualization to tensorboard for batch_idx=0
            logger.info("plotting episode end grid visualization to tensorboard")
            figure, _ = batch_envs.render()
            writer.add_figure("visualizations/end_of_episode", figure, visualization_step)
            
            # add end of episode, post sim vis
            ### assumes a batch idx of 0
            batch_envs.disease_engine.grids[0] = episode_end_post_sim_grid_states[0] # monkey patch grid state and render again - fix me
            figure, _ = batch_envs.render()
            writer.add_figure("visualizations/end_of_episode_post_sim", figure, visualization_step)
            
            # render video batch
            batch_0_episode_length = (~info["batch_dones"][:, done_subset_indices]).T[0].sum() + 1
            
            video_data_for_batch_0 = obs[:batch_0_episode_length, [0], :, :].unsqueeze(1).permute(2, 0, 1, 3, 4).repeat(1, 1, 3, 1, 1)
            writer.add_video("visualizations/batch_0_episode-grids", video_data_for_batch_0, visualization_step, fps=1, dataformats="NTCHW")    
            
            
            agent_position_video_tensor = torch.zeros((batch_0_episode_length, 1, *batch_envs.single_env_observation_space["infection_fields"].shape), device=device)
            time_indices = torch.arange(batch_0_episode_length, device=device)
            agent_position_video_tensor[time_indices, 0, agent_positions[:batch_0_episode_length, 0, 0].to(torch.long), agent_positions[:batch_0_episode_length, 0, 1].to(torch.long)] = 1         
            agent_position_video_tensor = agent_position_video_tensor.unsqueeze(1).permute(2, 0, 1, 3, 4).repeat(1, 1, 3, 1, 1)
            writer.add_video("visualizations/batch_0_episode-agent-location", agent_position_video_tensor, visualization_step, fps=1, dataformats="NTCHW")   
                        
            video_data_for_batch_0 = infection_fields[:batch_0_episode_length, [0], :, :].unsqueeze(1).permute(2, 0, 1, 3, 4).repeat(1, 1, 3, 1, 1)
            writer.add_video("visualizations/batch_0_episode-infection-field", video_data_for_batch_0, visualization_step, fps=1, dataformats="NTCHW")
            
            # also render the actions of the first corresponding batch
            actions_str = str(actions[:batch_0_episode_length, [0]].flatten().cpu().to(torch.int).tolist()) 
            writer.add_text("visualizations/batch_0_episode_actions", actions_str, visualization_step)
            
                
        ## rewards distribution
        if iteration % args.log_stats_every_n_iterations["rewards_distribution"] == 1:
            logger.info("plotting rewards distribution to tensorboard")
            writer.add_histogram("rewards/per-step-rewards", rewards.flatten(), global_step)

        ## actions distribution
        if iteration % args.log_stats_every_n_iterations["actions_distribution"] == 1:
            logger.info("plotting actions distribution to tensorboard")
            writer.add_histogram("distributions/actions", actions.flatten(), global_step)


        # bootstrap value if not done
        with torch.no_grad():
            next_value = agent.get_value((next_obs, next_infection_fields, next_vaccine_budgets, next_agent_positions)).reshape(1, -1)
            advantages = torch.zeros_like(rewards).to(device)
            lastgaelam = 0
            for t in reversed(range(args.num_steps)):
                if t == args.num_steps - 1:
                    nextnonterminal = 1.0 - next_done
                    nextvalues = next_value
                else:
                    nextnonterminal = 1.0 - dones[t + 1]
                    nextvalues = values[t + 1]
                delta = rewards[t] + args.gamma * nextvalues * nextnonterminal - values[t]
                advantages[t] = lastgaelam = delta + args.gamma * args.gae_lambda * nextnonterminal * lastgaelam
            returns = advantages + values

        # flatten the batch
        b_obs = obs.reshape((-1,) + batch_envs.single_env_observation_space["grids"].shape)
        b_infection_fields = infection_fields.reshape((-1,) + batch_envs.single_env_observation_space["infection_fields"].shape)
        b_vaccine_budgets = vaccine_budgets.reshape(-1)
        b_agent_positions = agent_positions.reshape((-1, 2)) # 2 coordinates
        
        b_logprobs = logprobs.reshape(-1)
        b_actions = actions.reshape((-1,) + batch_envs.single_env_action_space.shape)
        b_advantages = advantages.reshape(-1)
        b_returns = returns.reshape(-1)
        b_values = values.reshape(-1)
        b_rewards = rewards.reshape(-1)
        
        # Subsample non vaccination actions
        vaccination_actions_mask = b_actions == DiseaseEnvActions.VACCINATE.value
        non_vaccination_actions_mask = ~vaccination_actions_mask
        
        
        if args.ensure_num_vaccine_actions_is_equal_to_num_non_vaccine_actions:
            num_samples = min(int(non_vaccination_actions_mask.sum()), int(vaccination_actions_mask.sum()))
        
            # num_samples = int(non_vaccination_actions_mask.sum() * args.non_vaccination_action_subsampling_rate)
            sub_sampled_non_vaccination_actions_indices = torch.multinomial(
                    non_vaccination_actions_mask.to(torch.float),  # note the weights for the vaccination indices is 0 and for non vaccination indices is 1
                    num_samples = num_samples,
                    replacement=False
                )
            sub_sampled_vaccination_actions_indices = torch.multinomial(
                    vaccination_actions_mask.to(torch.float),  # note the weights for the vaccination indices is 0 and for non vaccination indices is 1
                    num_samples = num_samples,
                    replacement=False
                )
            
            subsampled_indices = torch.cat([
                sub_sampled_non_vaccination_actions_indices,
                sub_sampled_vaccination_actions_indices
            ]).to(torch.long)

            logger.info(f"Total sub sampled indices: {subsampled_indices.shape}")
            logger.info(f"Total sub sampled non vaccination indices: {sub_sampled_non_vaccination_actions_indices.shape}")
            logger.info(f"Total sampled vaccination indices: {sub_sampled_vaccination_actions_indices.shape}")
            
            if num_samples == 0: # if by chance the policy crases to either or action classes, just use all the samples
                subsampled_indices = torch.randperm(len(b_obs))
        else:       
            subsampled_indices = torch.randperm(len(b_obs))

            # vaccination_actions_indices = torch.nonzero(vaccination_actions_mask).squeeze(1)        
            # if num_samples > 0:
            #     sub_sampled_non_vaccination_actions_indices = torch.multinomial(
            #             non_vaccination_actions_mask.to(torch.float),  # note the weights for the vaccination indices is 0 and for non vaccination indices is 1
            #             num_samples = num_samples,
            #             replacement=False
            #         )
            #     subsampled_indices = torch.cat([
            #         sub_sampled_non_vaccination_actions_indices,
            #         vaccination_actions_indices
            #     ]).to(torch.long)            
            # else:
            #     subsampled_indices = vaccination_actions_indices
            


        # subsampled_indices = torch.randperm(len(subsampled_indices))
        
        
        ## sub sampled rewards distribution
        if iteration % args.log_stats_every_n_iterations["rewards_distribution"] == 1:
            logger.info("plotting subsampled rewards distribution to tensorboard")
            writer.add_histogram("distributions/rewards_subsampled", b_rewards[subsampled_indices].flatten(), global_step)
        
        effective_learning_step += len(subsampled_indices)
        writer.add_scalar("charts/effective_learning_steps", effective_learning_step, global_step)

        
        # Optimizing the policy and value network
        b_inds = np.arange(subsampled_indices.shape[0])
        batch_size = subsampled_indices.shape[0] // args.minibatch_size
        
        clipfracs = []
        for epoch in range(args.update_epochs):
            np.random.shuffle(b_inds)
            for start in range(0, batch_size, args.minibatch_size):
                end = start + args.minibatch_size
                # mb_inds = b_inds[start:end]
                mb_inds = subsampled_indices[start:end]
                
                _, newlogprob, entropy, newvalue, raw_logits = agent.get_action_and_value((b_obs[mb_inds], b_infection_fields[mb_inds], b_vaccine_budgets[mb_inds], b_agent_positions[mb_inds]), b_actions.long()[mb_inds], disable_action_masking=True)
                logratio = newlogprob - b_logprobs[mb_inds]
                ratio = logratio.exp()

                with torch.no_grad():
                    # calculate approx_kl http://joschu.net/blog/kl-approx.html
                    old_approx_kl = (-logratio).mean()
                    approx_kl = ((ratio - 1) - logratio).mean()
                    clipfracs += [((ratio - 1.0).abs() > args.clip_coef).float().mean().item()]

                mb_advantages = b_advantages[mb_inds]
                if args.norm_adv:
                    mb_advantages = (mb_advantages - mb_advantages.mean()) / (mb_advantages.std() + 1e-8)

                # Policy loss
                pg_loss1 = -mb_advantages * ratio
                pg_loss2 = -mb_advantages * torch.clamp(ratio, 1 - args.clip_coef, 1 + args.clip_coef)
                pg_loss = torch.max(pg_loss1, pg_loss2).mean()

                # Value loss
                newvalue = newvalue.view(-1)
                if args.clip_vloss:
                    v_loss_unclipped = (newvalue - b_returns[mb_inds]) ** 2
                    v_clipped = b_values[mb_inds] + torch.clamp(
                        newvalue - b_values[mb_inds],
                        -args.clip_coef,
                        args.clip_coef,
                    )
                    v_loss_clipped = (v_clipped - b_returns[mb_inds]) ** 2
                    v_loss_max = torch.max(v_loss_unclipped, v_loss_clipped)
                    v_loss = 0.5 * v_loss_max.mean()
                else:
                    v_loss = 0.5 * ((newvalue - b_returns[mb_inds]) ** 2).mean()

                entropy_loss = entropy.mean()
                loss = pg_loss - args.ent_coef * entropy_loss + v_loss * args.vf_coef

                optimizer.zero_grad()
                loss.backward()
                nn.utils.clip_grad_norm_(agent.parameters(), args.max_grad_norm)
                optimizer.step()

            if args.target_kl is not None and approx_kl > args.target_kl:
                break

        y_pred, y_true = b_values.cpu().numpy(), b_returns.cpu().numpy()
        var_y = np.var(y_true)
        explained_var = np.nan if var_y == 0 else 1 - np.var(y_true - y_pred) / var_y

        # TRY NOT TO MODIFY: record rewards for plotting purposes
        writer.add_scalar("charts/learning_rate", optimizer.param_groups[0]["lr"], global_step)
        writer.add_scalar("losses/value_loss", v_loss.item(), global_step)
        writer.add_scalar("losses/policy_loss", pg_loss.item(), global_step)
        writer.add_scalar("losses/entropy", entropy_loss.item(), global_step)
        writer.add_scalar("losses/old_approx_kl", old_approx_kl.item(), global_step)
        writer.add_scalar("losses/approx_kl", approx_kl.item(), global_step)
        writer.add_scalar("losses/clipfrac", np.mean(clipfracs), global_step)
        writer.add_scalar("losses/explained_variance", explained_var, global_step)
        print("SPS:", int(global_step / (time.time() - start_time)))
        print("effective-SPS:", int(effective_learning_step / (time.time() - start_time)))
        
        writer.add_scalar("charts/SPS", int(global_step / (time.time() - start_time)), global_step)
        writer.add_scalar("charts/effective-SPS", int(effective_learning_step / (time.time() - start_time)), global_step)


    # batch_envs.close() # close the environment
    writer.close()