import copy

import numpy as np
import torch
import torch.nn.functional as F
from loguru import logger

from disease_grids import DiseaseGrids
from disease_states import DiseaseState


class DiseaseEngine:
    def __init__(self, 
                 grid_size=10, 
                 batch_size=100, 
                 initial_population_distribution=None,
                 infection_probability=0.03, 
                 recovery_probability=0.02,
                 seed=None,
                 device="cuda" if torch.cuda.is_available() else "cpu"):
        """
        Initialize the DiseaseEngine class.

        Parameters:
        grid_size (int): Size of each grid (grid_size x grid_size).
        batch_size (int): Number of grids to simulate in parallel.
        initial_population_distribution (dict): Initial population distribution for each disease state. 
                                If fractions are provided, assume probability, else assume absolute numbers per grid.
                                It is the responsibility of the user to provide these numbers correctly
        infection_probability (float): Probability of infection for each step.
        recovery_probability (float): Probability of recovery for each step.
        seed (int): Random seed for reproducibility.
        device (str): Device to run the computations on ("cpu" or "cuda" or "cuda:0").
        """
        self.grid_size = grid_size
        self.batch_size = batch_size
        self.initial_population_distribution = initial_population_distribution or {
            DiseaseState.SUSCEPTIBLE: 2,
            DiseaseState.INFECTIOUS: 1,
            DiseaseState.RECOVERED: 1,
            DiseaseState.VACCINATED: 1
        }
        self.infection_probability = infection_probability
        self.recovery_probability = recovery_probability
        self.seed = seed
        self.device = torch.device(device)
        
        if self.seed:
            self.seed_everything(self.seed)
        
        self.instantiate_grids()
    
    def seed_everything(self, seed):
        """
        Seed all random number generators for reproducibility.
        """
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
        np.random.seed(seed)
            
    def instantiate_grids(self):
        """
        Instantiate the main grid object and initialize populations across different states.
        Returns:
        DiseaseGrids: The instantiated grids.
        """
        self.grids = DiseaseGrids(grids=None, batch_size=self.batch_size, grid_size=self.grid_size, device=self.device)
        
        # create a flat view of the grid for state assignment
        population_distribution = {}
        if np.sum(list(self.initial_population_distribution.values())) <= 1:
            # assume these are probability distributions
            for state, fraction in self.initial_population_distribution.items():
                population_distribution[state] = int(fraction * self.grid_size * self.grid_size)
                # convert fractional distribution to absolute numbers
        else:
            # assume these are absolute numbers
            population_distribution = self.initial_population_distribution
            # and less than the total number of cells in a grid in a single batch
            assert np.sum(list(population_distribution.values()))  <= self.grid_size * self.grid_size
            
        flat_grid = self.grids.grids.view(self.batch_size, self.grid_size*self.grid_size, len(DiseaseState))  # Flatten the grids across batches
        
        # Initialize populations across different states - sequentially
        current_index = 0
        for state, population_count in population_distribution.items():
            population_count = int(population_count)
            flat_grid[:, current_index:current_index+population_count] = self.grids.disease_state_vectors[state]
            current_index += population_count
        
        # shuffle each grid separately
        flat_grid = flat_grid[:, torch.randperm(self.grid_size*self.grid_size), :]
        
        # revert back to the original shape
        self.grids.grids = flat_grid.view(self.batch_size, self.grid_size, self.grid_size, len(DiseaseState))

        return self.grids
    
    def get_state(self):
        """
        returns the internal grid state as a torch tensor
        """
        return self.grids.grids
    
    def reset(self):
        """
        Resets the internal disease grid
        """
        del self.grids
        self.instantiate_grids()
    
    def calculate_infection_field(self, batch_subset_mask=None, num_disease_steps=5, collect_statistics=True):
        if batch_subset_mask is None:
            batch_subset_mask = slice(None)
        
        grids = self.grids
        # Deep copy the grids to avoid in-place modifications
        # and also enable N step look-ahead simulations
        grids_clone = grids.clone_grid(subset_mask=batch_subset_mask)
        
        infection_field = grids_clone.get_infection_field(
            infection_probability=self.infection_probability, num_disease_steps=num_disease_steps, collect_statistics=collect_statistics
        )
        percentage_individuals_by_states = grids_clone.calculate_percentage_of_individuals_in_all_disease_states(
            batch_subset_mask=batch_subset_mask
        )
        
        statistics = {}
        if collect_statistics:             
            statistics = {
                "percentage_individuals_by_states": percentage_individuals_by_states
            }
        
        return infection_field, grids_clone, statistics
        
        
    
    def propagate_disease_step(self, batch_subset_mask=None, num_disease_steps=1, collect_statistics=True, collect_optimal_vaccines_count=False, collect_immediate_infection_risk_mask=False):
        """
        Perform disease propagation steps and optionally collect statistics.

        Parameters:
        num_disease_steps (int): Number of disease propagation steps to perform.
        collect_statistics (bool): Whether to collect statistics during propagation.

        Returns:
        tuple: Updated grids and collected statistics (if any).
        """
                
        if batch_subset_mask is None:
            batch_subset_mask = slice(None)
        
        grids = self.grids
            
        # Deep copy the grids to avoid in-place modifications
        # and also enable N step look-ahead simulations
        grids_clone = grids.clone_grid(subset_mask=batch_subset_mask)
        
        # Initialize statistics buffers if needed
        if collect_statistics:
            statistics_buffers = {
                "percentage_individuals_by_states": [grids_clone.calculate_percentage_of_individuals_in_all_disease_states()],
            }
            if collect_optimal_vaccines_count:
                statistics_buffers["optimal_vaccines_count"] = [grids_clone.calculate_optimal_vaccines_count()]
            if collect_immediate_infection_risk_mask:
                statistics_buffers["immediate_infection_risk_mask"] = [grids_clone.calculate_immediate_infection_risk_mask()]
        
        # Perform disease propagation steps
        for _ in range(num_disease_steps):
            # Count infected neighbours
            infectious_neighbours_count = grids_clone.count_infected_neighbours()
            
            # Calculate infection probabilities for each cell
            infection_probabilities = 1 - (1 - self.infection_probability)**infectious_neighbours_count
            
            # Infect susceptible cells
            infection_candidates_mask = torch.rand_like(infection_probabilities) <= infection_probabilities
            susceptible_mask = grids_clone[:, :, :, DiseaseState.SUSCEPTIBLE.value] == 1
            grids_clone.set_state(susceptible_mask * infection_candidates_mask, DiseaseState.INFECTIOUS)
            
            # Recover infectious cells
            infectious_mask = grids_clone[:, :, :, DiseaseState.INFECTIOUS.value] == 1
            recovery_mask = torch.rand(grids_clone.batch_size, grids_clone.grid_size, grids_clone.grid_size, device=grids_clone.device) <= self.recovery_probability
            grids_clone.set_state(infectious_mask * recovery_mask, DiseaseState.RECOVERED)
            
            # Collect statistics if needed
            if collect_statistics:
                statistics_buffers["percentage_individuals_by_states"].append(
                    grids_clone.calculate_percentage_of_individuals_in_all_disease_states()
                )
                if collect_optimal_vaccines_count:
                    statistics_buffers["optimal_vaccines_count"].append(
                        grids_clone.calculate_optimal_vaccines_count()
                    )
                if collect_immediate_infection_risk_mask:
                    statistics_buffers["immediate_infection_risk_mask"].append(
                        grids_clone.calculate_immediate_infection_risk_mask()
                    )
        
        statistics = {}
        if collect_statistics:
            statistics = {
                "percentage_individuals_by_states": torch.stack(statistics_buffers["percentage_individuals_by_states"], dim=2),
            }
            if collect_optimal_vaccines_count:
                statistics["optimal_vaccines_count"] = torch.stack(statistics_buffers["optimal_vaccines_count"], dim=2)
                
            if collect_immediate_infection_risk_mask:
                statistics["immediate_infection_risk_mask"] = torch.stack(statistics_buffers["immediate_infection_risk_mask"], dim=0).permute(1, 2, 3, 0)
                    
            

        return grids_clone, statistics

    def visualize_grid(self, grids=None, batch_idx=0, view_points=None, highlight_cells=[], title_text=None, plt_show=True):
        """
        Visualize a grid for a specific batch.

        Parameters:
        grids (torch.Tensor): Grids to visualize. If None, use the internal grids.
        batch_idx (int): Index of the batch to visualize.
        highlight_cells (list): List of cells to highlight (not used in current implementation).
        plt_show (bool): Whether to show the plot.

        Returns:
        tuple: Figure and axis of the plot if plt_show is False.
        """
        
        if grids is None:
            grids = self.grids
        
        if view_points is not None:
            # get egocentric view wrt view points
            grids = grids.get_egocentric_view(view_points)
            # in ego-centric view the the view point is translated to the center of the grid
                
        return self.grids.visualize_grid(grids, batch_idx, highlight_cells, title_text, plt_show)

if __name__ == "__main__":
    
    device = "cuda" if torch.cuda.is_available() else "cpu"

    engine = DiseaseEngine(
                        grid_size=10, 
                        batch_size=100, 
                        initial_population_distribution = {
                            DiseaseState.SUSCEPTIBLE: 7,
                            DiseaseState.INFECTIOUS: 1,
                            DiseaseState.RECOVERED: 1,
                            DiseaseState.VACCINATED: 1
                        },
                        infection_probability=0.02, 
                        recovery_probability=0.01,
                        seed=19911706,
                        device=device)
    
        
        
    # Perform 4 steps of disease propagation and collect statistics
    updated_grids, statistics = engine.propagate_disease_step(num_disease_steps=4, collect_statistics=True)
    
    # Print statistics
    # print("Statistics of Disease States:")
    # print(statistics)
    
    # Visualize the first batch grid
    engine.visualize_grid(batch_idx=0)
    
    infection_field = engine.calculate_infection_field(num_disease_steps=5)
    print(f"Infection Field: {infection_field}")

    
    # Perform one step of disease propagation on a subset of grids and collect statistics
    batch_subset_mask = torch.arange(0, 4, device=device)
    future_grids_subset_clone, statistics = engine.propagate_disease_step(batch_subset_mask=batch_subset_mask, num_disease_steps=1, collect_statistics=True, collect_optimal_vaccines_count=True, collect_immediate_infection_risk_mask=True)
    assert future_grids_subset_clone.grids.shape[0] == 4
    
